#pragma once
#include "vulkan\vulkan.h"
#include <string>

#include "vulkan_deleter.h"
#include "vulkan_common.h"
#include "misc_helper_functions.h"
struct shader_module
{
	vulkan_deleter<VkShaderModule> shader_piece;
	VkPipelineShaderStageCreateInfo shader_create_info;
	static shader_module load(VkShaderStageFlagBits shader_stage, const std::string& filename, const global_vulkan_instance_state& vulkan_state)
	{
		auto shader_data = spawnx_vulkan::read_from_file(filename);
		shader_module shader{ {vulkan_state.logical_device, vkDestroyShaderModule}, {} };

		VkShaderModuleCreateInfo create_info{};
		create_info.sType		= VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		create_info.codeSize	= (uint32_t)shader_data.size();
		create_info.pCode		= (uint32_t*)shader_data.data();
		CHECK_VK_RESULT(vkCreateShaderModule(vulkan_state.logical_device, &create_info, nullptr, shader.shader_piece.replace()));

		shader.shader_create_info = {};
		shader.shader_create_info.sType		= VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shader.shader_create_info.stage		= shader_stage;
		shader.shader_create_info.module	= shader.shader_piece;
		shader.shader_create_info.pName		= "main";

		return shader;
	}
	static shader_module load_vertex_shader(const std::string& filename, const global_vulkan_instance_state& vulkan_state)
	{
		return load(VK_SHADER_STAGE_VERTEX_BIT, filename, vulkan_state);
	}
	static shader_module load_fragment_shader(const std::string& filename, const global_vulkan_instance_state& vulkan_state)
	{
		return load(VK_SHADER_STAGE_FRAGMENT_BIT, filename, vulkan_state);
	}
};