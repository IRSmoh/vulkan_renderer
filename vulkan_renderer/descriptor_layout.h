#pragma once
#include <vulkan\vulkan.h>

#include "vulkan_deleter.h"
#include "vulkan_common.h"
#include "global_vulkan_instance_state.h"

#include "descriptor_binding_data_buffer.h"
#include "descriptor_binding_image_buffer.h"
struct descriptor_set_layout
{
	//bindings do not have to be continguous nor in any real order.
	//aka you can have gabs + increasing/decreasing/mixed order for vertex and fragment
	//e.g......
	//vertex:
	//binding = 0
	//binding = 3
	//fragment:
	//binding = 1
	//binding = 5
	//and that's fine.

	vulkan_deleter<VkDescriptorSetLayout> set_layout;
	std::vector<descriptor_binding_layout_data> vertex_bindings;
	std::vector<descriptor_binding_layout_image> fragment_bindings;

	descriptor_set_layout(){}
	descriptor_set_layout(const global_vulkan_instance_state& vulkan_state)
	{
		set_layout = { vulkan_state.logical_device, vkDestroyDescriptorSetLayout };
	}
	static descriptor_set_layout create_set_layout(const std::vector<descriptor_binding_layout_data>& vertex_bindings, const std::vector<descriptor_binding_layout_image>& fragment_bindings, const global_vulkan_instance_state& vulkan_state, bool bindings_are_sorted = false)
	{
		descriptor_set_layout descriptor_set_layout(vulkan_state);
		descriptor_set_layout.vertex_bindings = vertex_bindings;
		descriptor_set_layout.fragment_bindings = fragment_bindings;

		if (!bindings_are_sorted)
		{
			auto sort_lambda = [](const auto& left, const auto& right) {	return left.binding < right.binding; };
			std::sort(descriptor_set_layout.vertex_bindings.begin(), descriptor_set_layout.vertex_bindings.end(), sort_lambda);
			std::sort(descriptor_set_layout.fragment_bindings.begin(), descriptor_set_layout.fragment_bindings.end(), sort_lambda);
		}

		//	VkDescriptorSetLayoutBinding uboLayoutBinding = {};
		//	uboLayoutBinding.binding			= <shader_binding_slot>;
		//	uboLayoutBinding.descriptorCount	= <size_of_descriptor_array>;
		//	uboLayoutBinding.descriptorType		= VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		//	uboLayoutBinding.pImmutableSamplers = nullptr;
		//	uboLayoutBinding.stageFlags			= VK_SHADER_STAGE_VERTEX_BIT;
		std::vector<VkDescriptorSetLayoutBinding> bindings;
		for (auto&& vertex_binding : descriptor_set_layout.vertex_bindings)
		{
			VkDescriptorSetLayoutBinding layout_binding{};
			layout_binding.binding = vertex_binding.binding;
			layout_binding.descriptorCount = vertex_binding.descriptor_array_size;
			layout_binding.descriptorType = vertex_binding.descriptor_type;
			layout_binding.pImmutableSamplers = nullptr;
			layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

			bindings.push_back(layout_binding);
		}
		for (auto&& fragment_binding : descriptor_set_layout.fragment_bindings)
		{
			VkDescriptorSetLayoutBinding layout_binding{};
			layout_binding.binding = fragment_binding.binding;
			layout_binding.descriptorCount = 1;	//not sure how array textures are handled, but I assume its only ever 1 bound image per sampler.
			layout_binding.descriptorType = fragment_binding.descriptor_type;
			layout_binding.pImmutableSamplers = nullptr;
			layout_binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

			bindings.push_back(layout_binding);
		}
		VkDescriptorSetLayoutCreateInfo layout_info{};
		layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layout_info.bindingCount = (uint32_t)bindings.size();
		layout_info.pBindings = bindings.data();

		CHECK_VK_RESULT(vkCreateDescriptorSetLayout(vulkan_state.logical_device, &layout_info, nullptr, descriptor_set_layout.set_layout.replace()));
		return descriptor_set_layout;
	}
	static descriptor_set_layout create_compute_set_layout(const std::vector<descriptor_binding_layout_data>& data_bindings, const std::vector<descriptor_binding_layout_image>& image_bindings, const global_vulkan_instance_state& vulkan_state)
	{
		descriptor_set_layout descriptor_layout = { vulkan_state };

		std::vector<VkDescriptorSetLayoutBinding> bindings;
		for (auto&& data_binding : data_bindings)
		{
			VkDescriptorSetLayoutBinding layout_binding{};
			layout_binding.binding = data_binding.binding;
			layout_binding.descriptorCount = data_binding.descriptor_array_size;
			layout_binding.descriptorType = data_binding.descriptor_type;
			layout_binding.pImmutableSamplers = nullptr;
			layout_binding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

			bindings.push_back(layout_binding);
		}
		for (auto&& image_binding : image_bindings)
		{
			VkDescriptorSetLayoutBinding layout_binding{};
			layout_binding.binding = image_binding.binding;
			layout_binding.descriptorCount = 1;	//not sure how array textures are handled, but I assume its only ever 1 bound image per sampler.
			layout_binding.descriptorType = image_binding.descriptor_type;
			layout_binding.pImmutableSamplers = nullptr;
			layout_binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

			bindings.push_back(layout_binding);
		}
		VkDescriptorSetLayoutCreateInfo layout_info{};
		layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layout_info.bindingCount = (uint32_t)bindings.size();
		layout_info.pBindings = bindings.data();

		CHECK_VK_RESULT(vkCreateDescriptorSetLayout(vulkan_state.logical_device, &layout_info, nullptr, descriptor_layout.set_layout.replace()));
		return descriptor_layout;
	}
};
inline VkDescriptorSetLayoutBinding descriptor_set_layout_binding(
	VkDescriptorType type,
	VkShaderStageFlags stageFlags,
	uint32_t binding,
	uint32_t descriptorCount = 1)
{
	VkDescriptorSetLayoutBinding setLayoutBinding{};
	setLayoutBinding.descriptorType = type;
	setLayoutBinding.stageFlags = stageFlags;
	setLayoutBinding.binding = binding;
	setLayoutBinding.descriptorCount = descriptorCount;
	return setLayoutBinding;
}
struct descriptor_set
{
	descriptor_set_layout layout;
	vulkan_deleter<VkDescriptorPool> descriptor_pool;
	VkDescriptorSet set;
	descriptor_set() {}
	descriptor_set(global_vulkan_instance_state& vulkan_state) : layout(vulkan_state)
	{
		descriptor_pool = { vulkan_state.logical_device, vkDestroyDescriptorPool };
	}
	//vertex and fragment buffers must be sorted in lowest to highest binding values.
	//e.g. if you're binding a uniform buffer in slot 0 and a camera in slot 1
	//the uniform buffer MUST come before the camera buffer.
	void create_descriptor_pool(const global_vulkan_instance_state& vulkan_state)
	{
		std::vector<VkDescriptorPoolSize> pool_sizes;
		for (const auto& vertex_binding : layout.vertex_bindings)
		{
			pool_sizes.push_back({ vertex_binding.descriptor_type, vertex_binding.descriptor_array_size });
		}
		for (const auto& fragment_binding : layout.fragment_bindings)
		{
			pool_sizes.push_back({ fragment_binding.descriptor_type, 1 });	//image array size is assumed one for the time being.
		}
		VkDescriptorPoolCreateInfo pool_info{};
		pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		pool_info.poolSizeCount = (uint32_t)pool_sizes.size();
		pool_info.pPoolSizes = pool_sizes.data();
		pool_info.maxSets = 1;

		CHECK_VK_RESULT(vkCreateDescriptorPool(vulkan_state.logical_device, &pool_info, nullptr, descriptor_pool.replace()));
	}
	static descriptor_set create_descriptor_set(descriptor_set_layout layout, global_vulkan_instance_state& state)
	{
		descriptor_set desc_set;
		desc_set.layout = layout;
		desc_set.create_descriptor_pool(state);
		
		std::vector<VkDescriptorSetLayout> layouts = { layout.set_layout };
		VkDescriptorSetAllocateInfo alloc_info{};
		alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		alloc_info.descriptorPool = desc_set.descriptor_pool;
		alloc_info.descriptorSetCount = (uint32_t)layouts.size();
		alloc_info.pSetLayouts = layouts.data();

		CHECK_VK_RESULT(vkAllocateDescriptorSets(state.logical_device, &alloc_info, &desc_set.set));
		return desc_set;
	}
	void write_descriptor_set(const std::vector<VkBuffer>& vertex_buffers, const std::vector<std::pair<VkSampler, VkImageView>>& fragment_buffers, global_vulkan_instance_state& vulkan_state)
	{
		assert(layout.vertex_bindings.size() == vertex_buffers.size());
		assert(layout.fragment_bindings.size() == fragment_buffers.size());

		std::vector<descriptor_binding_data_buffer>	vertex_bindings;
		std::vector<descriptor_binding_image_buffer>	fragment_bindings;

		vertex_bindings.reserve(vertex_buffers.size());
		fragment_bindings.reserve(fragment_buffers.size());

		for (size_t i = 0; i < vertex_buffers.size(); ++i)
		{
			vertex_bindings.push_back({ layout.vertex_bindings[i], vertex_buffers[i] });
		}
		for (size_t i = 0; i < fragment_buffers.size(); ++i)
		{
			fragment_bindings.push_back({ layout.fragment_bindings[i], fragment_buffers[i].first, fragment_buffers[i].second });
		}

		std::vector<VkDescriptorBufferInfo> vertex_buffer_infos;
		std::vector<VkDescriptorImageInfo> fragment_image_infos;
		std::vector<VkWriteDescriptorSet> descriptor_writes;

		vertex_buffer_infos.reserve(vertex_bindings.size());
		fragment_image_infos.reserve(fragment_bindings.size());

		for (const auto& vertex_binding : vertex_bindings)
		{
			VkDescriptorBufferInfo buffer_info{};
			buffer_info.buffer = vertex_binding.buffer;
			buffer_info.offset = 0; //???
			buffer_info.range = vertex_binding.layout.type_stride;

			vertex_buffer_infos.push_back(buffer_info);

			VkWriteDescriptorSet descriptor_write{};
			descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptor_write.dstSet = set;
			descriptor_write.dstBinding = vertex_binding.layout.binding;
			descriptor_write.dstArrayElement = 0; // ???????
			descriptor_write.descriptorType = vertex_binding.layout.descriptor_type;
			descriptor_write.descriptorCount = vertex_binding.layout.descriptor_array_size;
			descriptor_write.pBufferInfo = &vertex_buffer_infos.back();

			descriptor_writes.push_back(descriptor_write);
		}
		for (const auto& fragment_binding : fragment_bindings)
		{
			VkDescriptorImageInfo image_info{};
			image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			image_info.imageView = fragment_binding.image_view;
			image_info.sampler = fragment_binding.sampler;

			fragment_image_infos.push_back(image_info);

			VkWriteDescriptorSet descriptor_write{};
			descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptor_write.dstSet = set;
			descriptor_write.dstBinding = fragment_binding.layout.binding;
			descriptor_write.dstArrayElement = 0; // ??????????
			descriptor_write.descriptorType = fragment_binding.layout.descriptor_type;
			descriptor_write.descriptorCount = 1; // same logic as previous 1 for image related things (scroll up)
			descriptor_write.pImageInfo = &fragment_image_infos.back();

			descriptor_writes.push_back(descriptor_write);
		}

		vkUpdateDescriptorSets(vulkan_state.logical_device, (uint32_t)descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);
	}
	void create_descriptor_set(const std::vector<VkBuffer>& vertex_buffers, const std::vector<std::pair<VkSampler, VkImageView>>& fragment_buffers, global_vulkan_instance_state& vulkan_state)
	{
		create_descriptor_pool(vulkan_state);
		assert(layout.vertex_bindings.size() == vertex_buffers.size());
		assert(layout.fragment_bindings.size() == fragment_buffers.size());

		std::vector<descriptor_binding_data_buffer>	vertex_bindings;
		std::vector<descriptor_binding_image_buffer>	fragment_bindings;

		vertex_bindings.reserve(vertex_buffers.size());
		fragment_bindings.reserve(fragment_buffers.size());

		for (size_t i = 0; i < vertex_buffers.size(); ++i)
		{
			vertex_bindings.push_back({ layout.vertex_bindings[i], vertex_buffers[i] });
		}
		for (size_t i = 0; i < fragment_buffers.size(); ++i)
		{
			fragment_bindings.push_back({ layout.fragment_bindings[i], fragment_buffers[i].first, fragment_buffers[i].second });
		}

		std::vector<VkDescriptorSetLayout> layouts = { layout.set_layout };
		VkDescriptorSetAllocateInfo alloc_info{};
		alloc_info.sType				= VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		alloc_info.descriptorPool		= descriptor_pool;
		alloc_info.descriptorSetCount	= (uint32_t)layouts.size();
		alloc_info.pSetLayouts			= layouts.data();

		CHECK_VK_RESULT(vkAllocateDescriptorSets(vulkan_state.logical_device, &alloc_info, &set));

		std::vector<VkDescriptorBufferInfo> vertex_buffer_infos;
		std::vector<VkDescriptorImageInfo> fragment_image_infos;
		std::vector<VkWriteDescriptorSet> descriptor_writes;

		vertex_buffer_infos.reserve(vertex_bindings.size());
		fragment_image_infos.reserve(fragment_bindings.size());

		for (const auto& vertex_binding : vertex_bindings)
		{
			VkDescriptorBufferInfo buffer_info{};
			buffer_info.buffer	= vertex_binding.buffer;
			buffer_info.offset	= 0; //???
			buffer_info.range	= vertex_binding.layout.type_stride;

			vertex_buffer_infos.push_back(buffer_info);

			VkWriteDescriptorSet descriptor_write{};
			descriptor_write.sType				= VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptor_write.dstSet				= set;
			descriptor_write.dstBinding			= vertex_binding.layout.binding;
			descriptor_write.dstArrayElement	= 0; // ???????
			descriptor_write.descriptorType		= vertex_binding.layout.descriptor_type;
			descriptor_write.descriptorCount	= vertex_binding.layout.descriptor_array_size;
			descriptor_write.pBufferInfo		= &vertex_buffer_infos.back();

			descriptor_writes.push_back(descriptor_write);
		}
		for (const auto& fragment_binding : fragment_bindings)
		{
			VkDescriptorImageInfo image_info{};
			image_info.imageLayout	= VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			image_info.imageView	= fragment_binding.image_view;
			image_info.sampler		= fragment_binding.sampler;

			fragment_image_infos.push_back(image_info);

			VkWriteDescriptorSet descriptor_write{};
			descriptor_write.sType				= VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptor_write.dstSet				= set;
			descriptor_write.dstBinding			= fragment_binding.layout.binding;
			descriptor_write.dstArrayElement	= 0; // ??????????
			descriptor_write.descriptorType		= fragment_binding.layout.descriptor_type;
			descriptor_write.descriptorCount	= 1; // same logic as previous 1 for image related things (scroll up)
			descriptor_write.pImageInfo			= &fragment_image_infos.back();

			descriptor_writes.push_back(descriptor_write);
		}

		vkUpdateDescriptorSets(vulkan_state.logical_device, (uint32_t)descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);
	}

};