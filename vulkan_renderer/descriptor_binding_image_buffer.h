#pragma once
#include <vulkan\vulkan.h>

struct descriptor_binding_layout_image
{
	uint32_t binding;
	VkDescriptorType descriptor_type;
	static descriptor_binding_layout_image make_descriptor_layout(uint32_t binding, VkDescriptorType descriptor_type)
	{
		return { binding,descriptor_type };
	}
};
struct descriptor_binding_image_buffer
{
	descriptor_binding_layout_image layout;
	VkSampler sampler;
	VkImageView image_view;

	static descriptor_binding_image_buffer make_descriptor_buffer(descriptor_binding_layout_image layout, VkSampler sampler, VkImageView image_view)
	{
		return { layout, sampler, image_view };
	}
};