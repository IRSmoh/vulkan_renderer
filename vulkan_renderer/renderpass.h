#pragma once
#include <vulkan\vulkan.h>

#include "vulkan_common.h"
#include "vulkan_deleter.h"

#include "global_vulkan_instance_state.h"
struct renderpass
{
	vulkan_deleter<VkRenderPass> render_pass;
	renderpass() {}
	renderpass(const global_vulkan_instance_state& vulkan_state)
	{
		render_pass = { vulkan_state.logical_device, vkDestroyRenderPass };
	}
	static renderpass create_shadowmap_renderpass(const global_vulkan_instance_state& vulkan_state)
	{
		renderpass render_pass(vulkan_state);

		VkAttachmentDescription depth_attachment{};
		depth_attachment.format			= VK_FORMAT_D32_SFLOAT;
		depth_attachment.samples		= VK_SAMPLE_COUNT_1_BIT;
		depth_attachment.loadOp			= VK_ATTACHMENT_LOAD_OP_CLEAR;
		depth_attachment.storeOp		= VK_ATTACHMENT_STORE_OP_STORE;
		depth_attachment.stencilLoadOp	= VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depth_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depth_attachment.initialLayout	= VK_IMAGE_LAYOUT_UNDEFINED;
		depth_attachment.finalLayout	= VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		
		VkAttachmentReference depth_ref{};
		depth_ref.attachment	= 0;
		depth_ref.layout		= VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkSubpassDescription subpass{};
		subpass.pipelineBindPoint		= VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.pDepthStencilAttachment = &depth_ref;

		VkRenderPassCreateInfo renderpass_info{};
		renderpass_info.sType			= VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderpass_info.pNext			= nullptr;
		renderpass_info.attachmentCount = 1;
		renderpass_info.pAttachments	= &depth_attachment;
		renderpass_info.subpassCount	= 1;
		renderpass_info.pSubpasses		= &subpass;

		CHECK_VK_RESULT(vkCreateRenderPass(vulkan_state.logical_device, &renderpass_info, nullptr, render_pass.render_pass.replace()));
		return render_pass;
	}
	static renderpass create_renderpass(const global_vulkan_instance_state& vulkan_state)
	{
		renderpass render_pass(vulkan_state);
		VkAttachmentDescription color_attachment{};

		color_attachment.format			= vulkan_state.swapchain.image_format;
		color_attachment.samples		= VK_SAMPLE_COUNT_1_BIT;//vulkan_state.shader_details.sample_count;
		color_attachment.loadOp			= VK_ATTACHMENT_LOAD_OP_CLEAR;
		color_attachment.storeOp		= VK_ATTACHMENT_STORE_OP_STORE;

		color_attachment.stencilLoadOp	= VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		color_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

		color_attachment.initialLayout	= VK_IMAGE_LAYOUT_UNDEFINED;
		color_attachment.finalLayout	= VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentDescription depth_attachment{};
		depth_attachment.format			= vulkan_state.find_depth_format();
		depth_attachment.samples		= VK_SAMPLE_COUNT_1_BIT;
		depth_attachment.loadOp			= VK_ATTACHMENT_LOAD_OP_CLEAR;
		depth_attachment.storeOp		= VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depth_attachment.stencilLoadOp	= VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depth_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depth_attachment.initialLayout	= VK_IMAGE_LAYOUT_UNDEFINED;
		depth_attachment.finalLayout	= VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference color_attachment_ref{};
		color_attachment_ref.attachment = 0;
		color_attachment_ref.layout		= VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkAttachmentReference depth_attachment_ref{};
		depth_attachment_ref.attachment = 1;
		depth_attachment_ref.layout		= VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkSubpassDescription subpass{};
		subpass.pipelineBindPoint		= VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount	= 1;
		subpass.pColorAttachments		= &color_attachment_ref;
		subpass.pDepthStencilAttachment = &depth_attachment_ref;

		VkSubpassDependency dependency{};
		dependency.srcSubpass			= VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass			= 0;
		dependency.srcStageMask			= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask		= 0;
		dependency.dstStageMask			= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstAccessMask		= VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		std::array<VkAttachmentDescription, 2> attachments = { {color_attachment, depth_attachment} };
		VkRenderPassCreateInfo renderpass_info{};
		renderpass_info.sType			= VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderpass_info.attachmentCount	= (uint32_t)attachments.size();
		renderpass_info.pAttachments	= attachments.data();
		renderpass_info.subpassCount	= 1;
		renderpass_info.pSubpasses		= &subpass;
		renderpass_info.dependencyCount	= 1;
		renderpass_info.pDependencies	= &dependency;

		CHECK_VK_RESULT(vkCreateRenderPass(vulkan_state.logical_device, &renderpass_info, nullptr, render_pass.render_pass.replace()));
		return render_pass;
	}
};