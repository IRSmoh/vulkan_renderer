#pragma once
#include <vulkan/vulkan.hpp>
#include <iostream>

std::string error_string(VkResult errorCode)
{
	switch (errorCode)
	{
#define STR(r) case VK_ ##r: return #r
		STR(NOT_READY);
		STR(TIMEOUT);
		STR(EVENT_SET);
		STR(EVENT_RESET);
		STR(INCOMPLETE);
		STR(ERROR_OUT_OF_HOST_MEMORY);
		STR(ERROR_OUT_OF_DEVICE_MEMORY);
		STR(ERROR_INITIALIZATION_FAILED);
		STR(ERROR_DEVICE_LOST);
		STR(ERROR_MEMORY_MAP_FAILED);
		STR(ERROR_LAYER_NOT_PRESENT);
		STR(ERROR_EXTENSION_NOT_PRESENT);
		STR(ERROR_FEATURE_NOT_PRESENT);
		STR(ERROR_INCOMPATIBLE_DRIVER);
		STR(ERROR_TOO_MANY_OBJECTS);
		STR(ERROR_FORMAT_NOT_SUPPORTED);
		STR(ERROR_SURFACE_LOST_KHR);
		STR(ERROR_NATIVE_WINDOW_IN_USE_KHR);
		STR(SUBOPTIMAL_KHR);
		STR(ERROR_OUT_OF_DATE_KHR);
		STR(ERROR_INCOMPATIBLE_DISPLAY_KHR);
		STR(ERROR_VALIDATION_FAILED_EXT);
		STR(ERROR_INVALID_SHADER_NV);
#undef STR
	default:
		return "UNKNOWN_ERROR";
	}
}

//Macro to check vulkan return results
//basically if this is triggered everythings broken.
#define CHECK_VK_RESULT(result)					\
{												\
	if(result != VK_SUCCESS)					\
	{											\
		std::cout << "Error : VkResult is \"" << error_string(result) << "\" in " << __FILE__ << " at line" << __LINE__ << std::endl;\
		assert(result == VK_SUCCESS);			\
	}											\
}

struct queue_family_indices
{
	int32_t graphics_family;
	int32_t present_family;
	queue_family_indices() : graphics_family(-1), present_family(-1) {}
	queue_family_indices(int32_t graphics, int32_t present) : graphics_family(graphics), present_family(present) {}
	bool is_complete() { return graphics_family != -1 && present_family != -1; }
};
struct swapchain_support_details
{
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> present_modes;
	bool is_valid()
	{
		return !formats.empty() && !present_modes.empty();
	}
};