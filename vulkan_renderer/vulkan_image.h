#pragma once
#include "vulkan_renderer.h"
struct vulkan_image
{
	vulkan_deleter<VkImage> image;
	vulkan_deleter<VkImageView> image_view;
	vulkan_allocation_wrapper allocation;

	vulkan_image(){}
	template<typename global_vulkan_instance_state_type>
	vulkan_image(const global_vulkan_instance_state_type& vulkan_instance_state) :
		image		({ vulkan_instance_state.logical_device, vkDestroyImage }),
		image_view	({ vulkan_instance_state.logical_device, vkDestroyImageView })
	{}
	template<typename global_vulkan_instance_state_type>
	static vulkan_image create_image_2D(const image_data& image_data_, global_vulkan_instance_state_type& vulkan_instance_state)
	{
		vulkan_image image{ vulkan_instance_state };
		image.create_image_impl(image_data_, VK_IMAGE_VIEW_TYPE_2D, vulkan_instance_state);
		return image;
	}
	template<typename global_vulkan_instance_state_type>
	static vulkan_image create_depth_image(VkExtent2D depth_extent, global_vulkan_instance_state_type& vulkan_instance_state)
	{
		vulkan_image image{ vulkan_instance_state };
		VkFormat depth_format = VK_FORMAT_D32_SFLOAT;
		VkImageViewType view_type = VK_IMAGE_VIEW_TYPE_2D;
		VkImageType image_type = image_type_from_view(view_type);
		image.build_image({ depth_extent.width, depth_extent.height,1 }, 1, 1, image_type, VK_IMAGE_LAYOUT_UNDEFINED, depth_format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, vulkan_instance_state);

		image.create_image_view({}, view_type, depth_format, VK_IMAGE_ASPECT_DEPTH_BIT, vulkan_instance_state);

		//vulkan_instance_state.transition_image_layout(image.image, depth_format, VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
		return image;
	}
private:
	template<typename global_vulkan_instance_state_type>
	void create_image_impl(const image_data& image_data_, VkImageViewType view_type, global_vulkan_instance_state_type& vulkan_instance_state)
	{
		//note to self: format is the texture's format. e.g. 8bits per channel. or whatever the depth format styling is.
		VkImageType image_type = image_type_from_view(view_type);
		VkDeviceSize image_size = image_data_.size();


		gpu_data_buffer staging_buffer = gpu_data_buffer::create_staging_buffer(image_size, vulkan_instance_state);
		//TODO:
		//this will need to be tweaked to support texture arrays/mip levels, but for now I can't understand how that needs to be set up exactly and having a broken but working version is more desirable.
		VkImageSubresourceLayers image_subresource{};
		image_subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		image_subresource.mipLevel = 0;
		image_subresource.baseArrayLayer = 0;
		image_subresource.layerCount = 1;

		memcpy(staging_buffer.allocation.alloc.data, image_data_.texels.data, static_cast<size_t>(image_size));

		build_image(image_data_.extent(), image_data_.mip_levels, image_data_.array_layers,
			image_type, VK_IMAGE_LAYOUT_PREINITIALIZED, image_data_.vulkan_image_format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, vulkan_instance_state);

		vulkan_instance_state.transition_image_layout(image, image_data_.vulkan_image_format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

		vulkan_instance_state.copy_buffer_to_image(staging_buffer.buffer, image, image_data_.extent(), image_subresource);
		vulkan_instance_state.transition_image_layout(image, image_data_.vulkan_image_format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);


		create_image_view(image_data_, view_type, image_data_.vulkan_image_format, VK_IMAGE_ASPECT_COLOR_BIT, vulkan_instance_state);
	}
	template<typename global_vulkan_instance_state_type>
	void create_image_view(const image_data& img_data, VkImageViewType image_view_type, VkFormat format, VkImageAspectFlags aspect_flags, const global_vulkan_instance_state_type& vulkan_instance_state)
	{
		VkImageViewCreateInfo view_info{};
		view_info.sType		= VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		view_info.image		= image;
		view_info.viewType	= image_view_type;
		view_info.format	= format;

		view_info.subresourceRange.aspectMask		= aspect_flags;
		view_info.subresourceRange.baseMipLevel		= 0;
		view_info.subresourceRange.levelCount		= 1;
		view_info.subresourceRange.baseArrayLayer	= 0;
		view_info.subresourceRange.layerCount		= 1;
		CHECK_VK_RESULT(vkCreateImageView(vulkan_instance_state.logical_device, &view_info, nullptr, image_view.replace()));
	}
	template<typename global_vulkan_instance_state_type>
	void build_image(VkExtent3D image_dimensions, uint32_t mip_levels, uint32_t array_layers, VkImageType image_type, VkImageLayout initial_layout,
		VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage,
		 const global_vulkan_instance_state_type& vulkan_instance_state)
	{
		VkImageCreateInfo image_info = {};

		image_info.sType			= VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		image_info.imageType		= image_type; 
		image_info.extent			= image_dimensions;
		image_info.mipLevels		= mip_levels;
		image_info.arrayLayers		= array_layers;
		image_info.format			= format;
		image_info.tiling			= tiling;
		image_info.initialLayout	= initial_layout;
		image_info.usage			= usage;
		image_info.samples			= vulkan_instance_state.shader_details.sample_count;
		image_info.sharingMode		= VK_SHARING_MODE_EXCLUSIVE;

		CHECK_VK_RESULT(vkCreateImage(vulkan_instance_state.logical_device, &image_info, nullptr, image.replace()));

		VkMemoryRequirements mem_reqs;
		vkGetImageMemoryRequirements(vulkan_instance_state.logical_device, image, &mem_reqs);

		allocation = vk_allocator->allocate(mem_reqs.size, mem_reqs.alignment, mem_reqs.memoryTypeBits, VULKAN_MEMORY_USAGE_GPU_ONLY, VULKAN_ALLOCATION_TYPE_IMAGE_OPTIMAL);


		vkBindImageMemory(vulkan_instance_state.logical_device, image, allocation.alloc.device_memory, allocation.alloc.offset);
	}
	static VkImageType image_type_from_view(VkImageViewType view_type)
	{
		if (view_type == VK_IMAGE_VIEW_TYPE_1D || view_type == VK_IMAGE_VIEW_TYPE_1D_ARRAY)
			return VK_IMAGE_TYPE_1D;
		if (view_type == VK_IMAGE_VIEW_TYPE_2D || view_type == VK_IMAGE_VIEW_TYPE_2D_ARRAY)
			return VK_IMAGE_TYPE_2D;
		if(view_type == VK_IMAGE_VIEW_TYPE_3D || view_type == VK_IMAGE_VIEW_TYPE_CUBE || view_type == VK_IMAGE_VIEW_TYPE_CUBE_ARRAY)
			return VK_IMAGE_TYPE_3D;
		return VK_IMAGE_TYPE_MAX_ENUM;
	}
};