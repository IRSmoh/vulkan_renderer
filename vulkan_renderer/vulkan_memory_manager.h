#pragma once
#include "vulkan_renderer.h"

enum vulkan_memory_usage_e {
	VULKAN_MEMORY_USAGE_UNKNOWN,
	VULKAN_MEMORY_USAGE_GPU_ONLY,
	VULKAN_MEMORY_USAGE_CPU_ONLY,
	VULKAN_MEMORY_USAGE_CPU_TO_GPU,
	VULKAN_MEMORY_USAGE_GPU_TO_CPU,
	VULKAN_MEMORY_USAGES,
};

enum vulkan_allocation_type_e {
	VULKAN_ALLOCATION_TYPE_UNASSIGNED,
	VULKAN_ALLOCATION_TYPE_BUFFER,
	VULKAN_ALLOCATION_TYPE_IMAGE,
	VULKAN_ALLOCATION_TYPE_IMAGE_LINEAR,
	VULKAN_ALLOCATION_TYPE_IMAGE_OPTIMAL,
	VULKAN_ALLOCATION_TYPES,
};
constexpr size_t megabyte = 1024 * 1024;
constexpr size_t vk_device_local_memory_in_MB = 128 * megabyte;
constexpr size_t vk_host_visible_memory_in_MB = 64 * megabyte;

uint32_t find_memory_type_index(const uint32_t memory_type_bits, const vulkan_memory_usage_e usage, const gpu_info& gpu_stats)
{
	const VkPhysicalDeviceMemoryProperties& physical_mem_properties = gpu_stats.memory_properties;

	VkMemoryPropertyFlags required = 0;
	VkMemoryPropertyFlags preferred = 0;

	switch (usage)
	{
	case VULKAN_MEMORY_USAGE_GPU_ONLY:
		preferred |= VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		break;
	case VULKAN_MEMORY_USAGE_CPU_ONLY:
		required |= VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
		break;
	case VULKAN_MEMORY_USAGE_CPU_TO_GPU:
		required |= VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
		preferred |= VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		break;
	case VULKAN_MEMORY_USAGE_GPU_TO_CPU:
		required |= VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
		preferred |= VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT;
		break;
	default:
		throw std::runtime_error("Unknown memory usage when attempting to find cpu/gpu memory types");
	}

	bool once = false;
	uint32_t preferred_failed_index = -1;

	for (uint32_t i = 0; i < physical_mem_properties.memoryTypeCount; ++i)
	{
		if (((memory_type_bits >> i) & 1) == 0)
			continue;
		VkMemoryPropertyFlags properties = physical_mem_properties.memoryTypes[i].propertyFlags;
		if ((properties & required) != required)
			continue;
		if ((properties & preferred) != preferred)
		{
			if (!once)
			{
				once = true;
				preferred_failed_index = i;
			}
			continue;
		}
		return i;
	}
	return preferred_failed_index;
}
struct vk_allocation_t
{
	int				owning_pool_id;		//used by vulkan allocator for book keeping
	int				memory_type_index;	//same as above

	uint32_t		id;
	VkDeviceMemory	device_memory;
	VkDeviceSize	offset;
	VkDeviceSize	size;
	void*			data;

	vk_allocation_t() :
		owning_pool_id(0),
		memory_type_index(-1),
		id(0),
		device_memory(VK_NULL_HANDLE),
		offset(0),
		size(0),
		data(nullptr)
	{}
};
class vulkan_allocator;
template<typename vulkan_allocator_type>
struct vulkan_allocation_wrapper_templated
{
	vk_allocation_t alloc;
	vulkan_allocator_type* owner;	//owner is also used to signal the moved from state.
	vulkan_allocation_wrapper_templated() : alloc(), owner(nullptr)
	{}
	vulkan_allocation_wrapper_templated(vk_allocation_t alloc_, vulkan_allocator_type* own): alloc(alloc_),owner(own) 
	{}
	vulkan_allocation_wrapper_templated(const vulkan_allocation_wrapper_templated&) = delete;
	vulkan_allocation_wrapper_templated& operator=(const vulkan_allocation_wrapper_templated&) = delete;
	vulkan_allocation_wrapper_templated(vulkan_allocation_wrapper_templated&& right)
	{
		perform_cleanup();
		alloc = right.alloc;
		owner = right.owner;
		right.owner = nullptr;
	}
	vulkan_allocation_wrapper_templated& operator=(vulkan_allocation_wrapper_templated&& right)
	{
		perform_cleanup();
		alloc = right.alloc;
		owner = right.owner;
		right.owner = nullptr;
		return *this;
	}
	~vulkan_allocation_wrapper_templated()
	{
		perform_cleanup();
	}
	void perform_cleanup()
	{
		if (owner && alloc.memory_type_index != -1)
		{
			owner->free(alloc);
		}
	}
};
using vulkan_allocation_wrapper = vulkan_allocation_wrapper_templated<vulkan_allocator>;
class vulkan_memory_block
{
	friend class vulkan_allocator;

	struct chunk_t
	{
		uint32_t					id;
		VkDeviceSize				size;
		VkDeviceSize				offset;
		vulkan_allocation_type_e	type;
	};

	std::vector<chunk_t>	data_chunks;
	int32_t					owning_pool_id;
	uint32_t				next_block_id;
	uint32_t				memory_type_index;
	vulkan_memory_usage_e	usage;
	VkDeviceMemory			device_memory;
	VkDeviceSize			size;
	VkDeviceSize			allocated;
	void*					data;	//this is just mapped data; we don't control it directly.

	VkDevice logical_device = VK_NULL_HANDLE;

public:
	vulkan_memory_block(uint32_t memory_type_index_, VkDeviceSize size_, vulkan_memory_usage_e usage_, VkDevice logical_device_) :
		next_block_id(0),
		memory_type_index(memory_type_index_),
		usage(usage_),
		device_memory(VK_NULL_HANDLE),
		size(size_),
		allocated(0),
		logical_device(logical_device_)
	{}
	vulkan_memory_block(vulkan_memory_block&& right)
	{
		cleanup();

		data_chunks = std::move(right.data_chunks);
		owning_pool_id = right.owning_pool_id;
		next_block_id = right.next_block_id;
		memory_type_index = right.memory_type_index;
		usage = right.usage;
		device_memory = right.device_memory;
		right.device_memory = VK_NULL_HANDLE;
		size = right.size;
		allocated = right.allocated;
		data = right.data;
		right.data = nullptr;
		logical_device = right.logical_device;
	}
	vulkan_memory_block& operator=(vulkan_memory_block&& right)
	{
		cleanup();

		data_chunks = std::move(right.data_chunks);
		owning_pool_id = right.owning_pool_id;
		next_block_id = right.next_block_id;
		memory_type_index = right.memory_type_index;
		usage = right.usage;
		device_memory = right.device_memory;
		right.device_memory = VK_NULL_HANDLE;
		size = right.size;
		allocated = right.allocated;
		data = right.data;
		right.data = nullptr;
		logical_device = right.logical_device;

		return *this;
	}
	~vulkan_memory_block()
	{
		cleanup();
	}
private:
	void cleanup()
	{
		if (logical_device == VK_NULL_HANDLE)
			return;
		if (is_host_visible() && device_memory != VK_NULL_HANDLE)
			vkUnmapMemory(logical_device, device_memory);
		if (device_memory != VK_NULL_HANDLE)
			vkFreeMemory(logical_device, device_memory, nullptr);
		device_memory = VK_NULL_HANDLE;
	}
public:
	//only reason I'm leaving this init alone is due to it returning a status.
	//makes no sense to check a constructor for a status...
	bool init()
	{
		if (memory_type_index == std::numeric_limits<uint32_t>::max())
			return false;

		VkMemoryAllocateInfo memory_alloc_info{};
		memory_alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		memory_alloc_info.allocationSize = size;
		memory_alloc_info.memoryTypeIndex = memory_type_index;

		CHECK_VK_RESULT(vkAllocateMemory(logical_device, &memory_alloc_info, nullptr, &device_memory));

		if (device_memory == VK_NULL_HANDLE)
			return false;
		if (is_host_visible())
		{
			CHECK_VK_RESULT(vkMapMemory(logical_device, device_memory, 0, size, 0, &data));
		}

		chunk_t head;
		head.id = next_block_id++;
		head.size = size;
		head.offset = 0;
		head.type = VULKAN_ALLOCATION_TYPE_UNASSIGNED;
		data_chunks.push_back(head);
		return true;
	}
	bool is_host_visible() const
	{
		return usage != VULKAN_MEMORY_USAGE_GPU_ONLY;
	}
private:
	//shamelessly stolen

	/*
	=============
	IsOnSamePage

	Algorithm comes from the Vulkan 1.0.39 spec. "Buffer-Image Granularity"
	Also known as "Linear-Optimal Granularity"
	=============
	*/
	static bool IsOnSamePage(
		VkDeviceSize rAOffset, VkDeviceSize rASize,
		VkDeviceSize rBOffset, VkDeviceSize pageSize) {

		assert(rAOffset + rASize <= rBOffset && rASize > 0 && pageSize > 0);

		VkDeviceSize rAEnd = rAOffset + rASize - 1;
		VkDeviceSize rAEndPage = rAEnd & ~(pageSize - 1);
		VkDeviceSize rBStart = rBOffset;
		VkDeviceSize rBStartPage = rBStart & ~(pageSize - 1);

		return rAEndPage == rBStartPage;
	}

	/*
	=============
	HasGranularityConflict

	Check that allocation types obey buffer image granularity.
	=============
	*/
	static bool HasGranularityConflict(vulkan_allocation_type_e type1, vulkan_allocation_type_e type2) {
		if (type1 > type2) {
			std::swap(type1, type2);
		}

		switch (type1) {
		case VULKAN_ALLOCATION_TYPE_UNASSIGNED:
			return false;
		case VULKAN_ALLOCATION_TYPE_BUFFER:
			return	type2 == VULKAN_ALLOCATION_TYPE_IMAGE ||
				type2 == VULKAN_ALLOCATION_TYPE_IMAGE_OPTIMAL;
		case VULKAN_ALLOCATION_TYPE_IMAGE:
			return  type2 == VULKAN_ALLOCATION_TYPE_IMAGE ||
				type2 == VULKAN_ALLOCATION_TYPE_IMAGE_LINEAR ||
				type2 == VULKAN_ALLOCATION_TYPE_IMAGE_OPTIMAL;
		case VULKAN_ALLOCATION_TYPE_IMAGE_LINEAR:
			return type2 == VULKAN_ALLOCATION_TYPE_IMAGE_OPTIMAL;
		case VULKAN_ALLOCATION_TYPE_IMAGE_OPTIMAL:
			return false;
		default:
			assert(false);
			return true;
		}
	}
public:
	//allocation struct must be supplied; and is overwritten by function end.
	bool allocate(VkDeviceSize size, VkDeviceSize align, VkDeviceSize granularity, vulkan_allocation_type_e alloc_type, vk_allocation_t& allocation)
	{
		const VkDeviceSize free_size = this->size - allocated;
		if (free_size < size)
			return false;
		auto best_fit = data_chunks.end();

		VkDeviceSize padding = 0;
		VkDeviceSize offset = 0;
		VkDeviceSize aligned_size = 0;

		auto force_align = [](VkDeviceSize x, VkDeviceSize a) {return((x)+((a)-1)) & ~((a)-1); };

		for (auto current = data_chunks.begin(), previous = data_chunks.end(); current != data_chunks.end(); previous = current, ++current)
		{
			if (current->type != VULKAN_ALLOCATION_TYPE_UNASSIGNED)
				continue;
			if (size > current->size)
				continue;

			offset = force_align(current->offset, align);

			if (previous != data_chunks.end() && granularity > 1)
				if (IsOnSamePage(previous->offset, previous->size, offset, granularity))
					if (HasGranularityConflict(previous->type, alloc_type))
						offset = force_align(offset, granularity);

			padding = offset - current->offset;
			aligned_size = padding + size;

			if (aligned_size > current->size)
				continue;
			if (aligned_size + allocated >= this->size)
				return false;

			auto next = current + 1;
			if (granularity > 1 && next != data_chunks.end())
			{
				if (IsOnSamePage(offset, size, next->offset, granularity))
					if (HasGranularityConflict(alloc_type, next->type))
						continue;
			}

			best_fit = current;
			break;
		}

		if (best_fit == data_chunks.end())
			return false;

		size_t best_fit_offset = best_fit - data_chunks.begin();
		if (best_fit->size > size)
		{
			chunk_t chunk = {};

			chunk.id = next_block_id++;
			chunk.size = best_fit->size - aligned_size;
			chunk.offset = offset + size;
			chunk.type = VULKAN_ALLOCATION_TYPE_UNASSIGNED;

			//put our new node immediately after the best fit; this implicitly stores the single stream tree structure (although its plausible this is expensive)
			data_chunks.insert(best_fit + 1, chunk);
		}
		best_fit = data_chunks.begin() + best_fit_offset;
		best_fit->type = alloc_type;
		best_fit->size = size;
		allocated += aligned_size;

		allocation.size = best_fit->size;
		allocation.id = best_fit->id;
		allocation.device_memory = device_memory;
		allocation.offset = offset;

		//book-keeping so we can know what owns us.
		allocation.owning_pool_id = owning_pool_id;
		allocation.memory_type_index = memory_type_index;

		if (is_host_visible())
			allocation.data = (char*)data + offset;

		return true;
	}
	void free(vk_allocation_t& allocation)
	{
		//sorting the data_chunks doesn't make a ton of sense..
		//iterating over a hash map removes the linearity that's expected.....
		//is there no good way of doing this? we could have thousands of allocations and several hundred deletions thats suddenly 100k traversals for nothing.
		//ordering is unlikely to be preserved due to id's being potentially out of order anyway (no binary searching)
		//well when it becomes a problem at some future point these are my thoughts ^^^^
		auto current = data_chunks.begin();
		for (; current != data_chunks.end(); ++current)
			if (current->id == allocation.id)
				break;
		if (current == data_chunks.end())
		{
			//some form of warning code
			return;
		}

		current->type = VULKAN_ALLOCATION_TYPE_UNASSIGNED;
		if (current != data_chunks.begin())
		{
			auto prev = current - 1;
			if (prev->type == VULKAN_ALLOCATION_TYPE_UNASSIGNED)
			{
				//merge the types together

				//the previous is now supposed to have a larger block of data.
				prev->size += current->size;

				data_chunks.erase(current);
				//reset ourselves at the prior node and continue.
				current = prev;
			}
		}
		auto next = current + 1;
		if (next != data_chunks.end() && next->type == VULKAN_ALLOCATION_TYPE_UNASSIGNED)
		{
			current->size += next->size;
			//in any case; we should try and fold this into the above ^^^^ in some way so we're not moving every single node ever 2x for no reason.
			data_chunks.erase(next);
		}
		allocated -= allocation.size;
	}
};

class vulkan_allocator
{
	int next_pool_id;
	int garbage_index;
	int device_local_memory_bytes;
	int host_visible_memory_bytes;
	VkDeviceSize buffer_image_granularity;

	const gpu_info* gpu_stats;
	VkDevice logical_device = VK_NULL_HANDLE;

	std::array<std::unordered_map<int, vulkan_memory_block>, VK_MAX_MEMORY_TYPES> blocks;
	std::array<std::vector<vk_allocation_t>,2> garbage_nodes;	//the size of the vector should match  the number of buffered frames we're using. e.g. typically 2-3.

public:
	vulkan_allocator() {}
	vulkan_allocator(const gpu_info& gpu_stats_, const VkDevice& logical_device_) :
		next_pool_id(0),
		garbage_index(0),
		device_local_memory_bytes(vk_device_local_memory_in_MB),
		host_visible_memory_bytes(vk_host_visible_memory_in_MB),
		buffer_image_granularity(gpu_stats_.properties.limits.bufferImageGranularity),
		gpu_stats(&gpu_stats_),
		logical_device(logical_device_)
	{}

	vulkan_allocation_wrapper allocate(VkDeviceSize size, VkDeviceSize align, uint32_t memory_type_bits, vulkan_memory_usage_e usage, vulkan_allocation_type_e alloc_type)
	{
		vk_allocation_t allocation;
		uint32_t memory_type_index = find_memory_type_index(memory_type_bits, usage, *gpu_stats);
		if (memory_type_index == std::numeric_limits<uint32_t>::max())
			throw std::runtime_error("vulkan_allocator::allocate: Failed to find valid memory_type_index for allocation request.");
		auto& block_group = blocks[memory_type_index];
		for (auto& block : block_group)
		{
			if (block.second.memory_type_index != memory_type_index)
				continue;
			if (block.second.allocate(size, align, buffer_image_granularity, alloc_type, allocation))
			{
				return { allocation, this };
			}
		}

		VkDeviceSize block_size = (usage == VULKAN_MEMORY_USAGE_GPU_ONLY) ? device_local_memory_bytes : host_visible_memory_bytes;

		vulkan_memory_block block = vulkan_memory_block(memory_type_index, block_size, usage, logical_device);
		if (block.init())
		{
			block.owning_pool_id = next_pool_id++;
			block_group.emplace( block.owning_pool_id, std::move(block) );	//we need an id for these...
		}
		else
			throw std::runtime_error("vulkan_allocator::allocate: Could not allocate new memory block.");
		auto& block_ = block_group.find(next_pool_id - 1)->second;
		block_.allocate(size, align, buffer_image_granularity, alloc_type, allocation);
		return { allocation, this };
	}
	//wouldn't this make more sense to just be a call to std::vector::erase?
	//well..... not sure since that's not an iterator being passed in
	void free(const vk_allocation_t& allocation)
	{
		garbage_nodes[garbage_index].push_back(allocation);
	}

	void empty_garbage()
	{
		constexpr int NUM_FRAME_BUFFERING = 2;
		//this reads as insanity but I'm putting it in anyway.
		//garbage_index = (garbage_index + 1) % NUM_FRAME_BUFFERING;
		auto& garbage = garbage_nodes[garbage_index];

		for (auto& allocation : garbage)
		{
			auto& block_map = blocks[allocation.memory_type_index];
			auto itr = block_map.find(allocation.owning_pool_id);
			if (itr != block_map.end())
			{
				itr->second.free(allocation);
				if (itr->second.allocated == 0)
				{
					block_map.erase(itr);
				}
			}
		}
		garbage.clear();
		garbage.shrink_to_fit();
	}
};
vulkan_allocator* vk_allocator;