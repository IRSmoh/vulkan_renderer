#pragma once
#include "vulkan_renderer.h"
struct vertex
{
	glm::vec3 pos;
	glm::vec3 normals;
	glm::vec2 uvs;
	constexpr static uint32_t vertex_binding_location = 0;
	constexpr static std::array<VkVertexInputBindingDescription,3> get_binding_description()
	{
		VkVertexInputBindingDescription binding_description{};
		binding_description.binding = 0;
		binding_description.stride = sizeof(glm::vec3);
		binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return { {	VkVertexInputBindingDescription{0,sizeof(pos), VK_VERTEX_INPUT_RATE_VERTEX},
					VkVertexInputBindingDescription{1,sizeof(normals), VK_VERTEX_INPUT_RATE_VERTEX},
					VkVertexInputBindingDescription{2,sizeof(uvs), VK_VERTEX_INPUT_RATE_VERTEX} } };
	}
	constexpr static std::array<VkVertexInputAttributeDescription, 3> get_attribute_descriptions()
	{

		//location	//our offset in the shader object aka (location = 0, location =1...)
		//binding	//must match the vertex buffer binding we supply our pipeline.
		//format	//our data format for the gpu.
		//stride	//the stride on the cpu and gpu must match.
		VkVertexInputAttributeDescription pos_attrib{}, normal_attrib{}, uv_attrib{};

		pos_attrib.location	= 0;
		pos_attrib.binding	= 0;
		pos_attrib.format	= VK_FORMAT_R32G32B32_SFLOAT;
		pos_attrib.offset	= 0;

		normal_attrib.location	= 1;
		normal_attrib.binding	= 1;
		normal_attrib.format	= VK_FORMAT_R32G32B32_SFLOAT;
		normal_attrib.offset	= 0;

		uv_attrib.location	= 2;
		uv_attrib.binding	= 2;
		uv_attrib.format	= VK_FORMAT_R32G32_SFLOAT;
		uv_attrib.offset	= 0;

		return {pos_attrib, normal_attrib, uv_attrib };
	}
};
struct shadow_vertex
{
	glm::vec3 pos;
	constexpr static std::array<VkVertexInputBindingDescription, 1> get_binding_description()
	{
		return { {VkVertexInputBindingDescription{	0,									//binding
													sizeof(pos),						//stride
													VK_VERTEX_INPUT_RATE_VERTEX}} };	//input rate
	}
	constexpr static std::array<VkVertexInputAttributeDescription, 1> get_attribute_descriptions()
	{
		return { VkVertexInputAttributeDescription{
			0,
			0,
			VK_FORMAT_R32G32B32_SFLOAT,
			0
		} };
	}
};