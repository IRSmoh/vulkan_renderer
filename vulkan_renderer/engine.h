#pragma once
#include "vulkan_renderer.h"
#include "im_gui.h"
#include <chrono>
namespace spawnx_vulkan
{
	struct pipeline_with_targets
	{
		pipeline pipeline;
		std::vector<std::weak_ptr<model>> models;
	};
	struct engine
	{
		global_vulkan_instance_state vulkan_instance;
		descriptor_set_layout descr_set_layout{ vulkan_instance };

		descriptor_set_layout shadow_descriptor_set_layout{ vulkan_instance };

		bool rerecord_commandbuffers;

		std::vector<pipeline_with_targets> pipelines;
		std::vector<std::weak_ptr<camera>> cameras{};
		std::weak_ptr<camera> shadow_camera;
		std::shared_ptr<model> model_1;
		renderpass render_pass;

		renderpass shadow_renderpass;
		vulkan_image shadow_image;
		framebuffer shadow_framebuffer;
		VkExtent2D shadow_extent;
		pipeline shadow_pipeline;
		
		std::chrono::time_point<std::chrono::high_resolution_clock> start{}, end{};

		vulkan_image_manager owned_image_manager;
		vulkan_sampler_manager owned_sampler_manager;

		vulkan_allocator vulkan_allocator_;

		std::function<void(int, int)> user_screen_resize_fnc;

		imgui_manager gui_manager;
		~engine()
		{
		}
		void startup(bool start_in_fullscreen)
		{
			rerecord_commandbuffers = false;
			vulkan_instance.engine_casting_fnc = &engine_recast_fnc;
			vulkan_instance.engine_ptr = this;

			image_manager = &owned_image_manager;
			sampler_manager = &owned_sampler_manager;

			vulkan_instance.vulkan_startup(start_in_fullscreen);

			vulkan_allocator_ = vulkan_allocator{ vulkan_instance.gpu_stats, vulkan_instance.logical_device };
			vk_allocator = &vulkan_allocator_;

			vulkan_instance.create_command_pool();
			vulkan_instance.create_swapchain();

			vulkan_instance.create_swapchain_image_views();
			vulkan_instance.create_depth_image();

			vulkan_instance.create_semaphores();
			/*
			init_window(start_in_fullscreen);
			create_instance();
			setup_debug_callback();
			create_surface();
			get_physical_device();
			get_gpu_stats();
			create_logical_device();
			*/
			//as a separate periodically updating thing:
			//create/load shaders
			//shader_module::load_vertex_shader("path", vulkan_instance);
			//create descriptor sets


			render_pass = renderpass::create_renderpass(vulkan_instance);
			shadow_renderpass = renderpass::create_shadowmap_renderpass(vulkan_instance);
			constexpr int shadow_size = 1024;
			shadow_extent = { shadow_size,shadow_size };
			shadow_image = vulkan_image::create_depth_image(shadow_extent, vulkan_instance);
			shadow_framebuffer = framebuffer::create_framebuffer(shadow_image, shadow_extent.width, shadow_extent.height, shadow_renderpass, vulkan_instance);
			//note to self: buffers do not give a shit about their binding. only the descriptor bindings care about 'where' the buffer gets its binding number.
			pipeline_creation();
			vulkan_instance.create_framebuffers(render_pass.render_pass);

			init_gui();
		}
		void pipeline_creation()
		{
			const std::string vertex_filename{ "shaders/terrain/shader.vert.spv" };
			const std::string fragment_filename{ "shaders/terrain/shader.frag.spv" };
			const std::string shadow_vert_filename{ "shaders/terrain/shadow_shader.vert.spv" };

			std::vector<descriptor_binding_layout_data> vertex_layouts{ descriptor_binding_layout_data::make_descriptor_layout<glm::mat4>(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER),
				descriptor_binding_layout_data::make_descriptor_layout<glm::mat4>(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER) };
			std::vector<descriptor_binding_layout_data> normal_render_layouts = vertex_layouts;
			normal_render_layouts.push_back(descriptor_binding_layout_data::make_descriptor_layout<glm::mat4>(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER));
			std::vector<descriptor_binding_layout_image> fragment_layouts{ descriptor_binding_layout_image::make_descriptor_layout(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER), descriptor_binding_layout_image::make_descriptor_layout(4,VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER) };

			descr_set_layout = descriptor_set_layout::create_set_layout(normal_render_layouts, fragment_layouts, vulkan_instance);
			auto pipeline = pipeline::create_pipeline<vertex>(vertex_filename, fragment_filename, descr_set_layout, render_pass.render_pass, VK_POLYGON_MODE_FILL, vulkan_instance);
			pipelines.push_back({ std::move(pipeline),{} });
			pipelines.push_back({ pipeline::create_pipeline<vertex>(vertex_filename, fragment_filename, descr_set_layout, render_pass.render_pass, VK_POLYGON_MODE_LINE, vulkan_instance), {} });

			shadow_descriptor_set_layout = descriptor_set_layout::create_set_layout(vertex_layouts, {}, vulkan_instance);
			shadow_pipeline = pipeline::create_shadow_pipeline<shadow_vertex>(shadow_vert_filename, shadow_descriptor_set_layout, shadow_renderpass.render_pass, vulkan_instance);

			
		}
		//call after regular init() since we need a valid render pass to hook up to.
		void init_gui()
		{
			gui_manager.gui_init({ vulkan_instance.window_width, vulkan_instance.window_height });
			gui_manager.init_resources(render_pass.render_pass, "shaders/ui.vert.spv", "shaders/ui.frag.spv", vulkan_instance);
			gui_manager.setup_imgui_physical_inputs(vulkan_instance.window);
		}
		void descriptor_set_creation()
		{
			model_1 = std::make_shared<model>(model{ descr_set_layout,shadow_descriptor_set_layout, vulkan_instance });
			model_1->make_default(vulkan_instance);

			if (cameras.empty())
				throw std::runtime_error("Specify at least one camera and add it to engine.cameras.");
			model_1->set_cameras(*cameras.back().lock(), *shadow_camera.lock());
			model_1->shadow_view = shadow_image.image_view;
			model_1->upload_data(vulkan_instance);

			model_1->position_matrix = glm::translate(glm::mat4(1), glm::vec3(10, 1, 0));
			pipelines[0].models.push_back(std::move(model_1));
		}
		//for right now we're assuming everything uses the same default pipeline literally because I dont want to deal with it. thats it.
		//TODO: fix this absolute ass.
		void add_model(std::weak_ptr<model> _model)
		{
			rerecord_commandbuffers = true;
			pipelines[0].models.push_back(std::move(_model));
		}
		void add_camera(std::weak_ptr<camera> cam)
		{
			cameras.push_back(cam);
		}
		void add_shadow_camera(std::weak_ptr<camera> shadow_cam)
		{
			shadow_camera = shadow_cam;
		}
		void update_commandbuffers()
		{
			if (vulkan_instance.command_buffers.size() > 0)
			{
				vkFreeCommandBuffers(vulkan_instance.logical_device, vulkan_instance.command_pool, (uint32_t)vulkan_instance.command_buffers.size(), vulkan_instance.command_buffers.data());
			}
			vulkan_instance.command_buffers.resize(vulkan_instance.swapchain.framebuffers.size()*2);	//the naive lazy way of setting up shadow's command buffers.

			VkCommandBufferAllocateInfo buffer_alloc_info{};
			buffer_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			buffer_alloc_info.commandPool = vulkan_instance.command_pool;
			buffer_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
			buffer_alloc_info.commandBufferCount = (uint32_t)vulkan_instance.command_buffers.size();

			CHECK_VK_RESULT(vkAllocateCommandBuffers(vulkan_instance.logical_device, &buffer_alloc_info, vulkan_instance.command_buffers.data()));

			for (size_t i = vulkan_instance.command_buffers.size() / 2; i < vulkan_instance.command_buffers.size(); ++i)
			{
				VkCommandBuffer curr_command_buffer = vulkan_instance.command_buffers[i];
				VkCommandBufferBeginInfo begin_info{};
				begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

				vkBeginCommandBuffer(curr_command_buffer, &begin_info);
				VkClearValue shadow_clear{};
				shadow_clear.depthStencil = { 1.f,0 };

				VkRenderPassBeginInfo renderpass_info{};
				renderpass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
				renderpass_info.renderPass = shadow_renderpass.render_pass;
				renderpass_info.framebuffer = shadow_framebuffer.buffer;
				renderpass_info.renderArea.offset = { 0,0 };
				renderpass_info.renderArea.extent = shadow_extent;
				renderpass_info.clearValueCount = 1;
				renderpass_info.pClearValues = &shadow_clear;

				VkViewport shadow_viewport{};
				shadow_viewport.height = (float)shadow_extent.height;
				shadow_viewport.width = (float)shadow_extent.width;
				shadow_viewport.minDepth = 0.f;
				shadow_viewport.maxDepth = 1.f;
				shadow_viewport.x = 0;
				shadow_viewport.y = 0;

				VkRect2D scissor;
				scissor.extent = shadow_extent;
				scissor.offset = { 0,0 };

				vkCmdSetViewport(curr_command_buffer, 0, 1, &shadow_viewport);
				vkCmdSetScissor(curr_command_buffer, 0, 1, &scissor);

				vkCmdBeginRenderPass(curr_command_buffer, &renderpass_info, VK_SUBPASS_CONTENTS_INLINE);

				vkCmdBindPipeline(curr_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shadow_pipeline.graphics_pipeline);
				auto& pipeline = pipelines[0];
				for (auto&& model_weak : pipeline.models)
				{
					if (auto model = model_weak.lock())
					{
						model->draw_shadows(curr_command_buffer, shadow_pipeline.pipeline_layout);
					}
				}
				vkCmdEndRenderPass(curr_command_buffer);
				CHECK_VK_RESULT(vkEndCommandBuffer(curr_command_buffer));
			}
			for (size_t i = 0; i < vulkan_instance.command_buffers.size()/2; ++i)
			{
				VkCommandBuffer curr_command_buffer = vulkan_instance.command_buffers[i];
				VkCommandBufferBeginInfo begin_info{};
				begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

				vkBeginCommandBuffer(curr_command_buffer, &begin_info);
				std::array<VkClearValue, 2> clear_color;
				clear_color[0].color = { 0.4f, 0.6f, 0.9f, 1.0f };
				clear_color[1].depthStencil = { 1.f,0 };

				VkRenderPassBeginInfo renderpass_info{};
				renderpass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
				renderpass_info.renderPass = render_pass.render_pass;	//oh god
				renderpass_info.framebuffer = vulkan_instance.swapchain.framebuffers[i];

				renderpass_info.renderArea.offset = { 0,0 };
				renderpass_info.renderArea.extent = vulkan_instance.swapchain.extent;

				renderpass_info.clearValueCount = (uint32_t)clear_color.size();
				renderpass_info.pClearValues = clear_color.data();


				vkCmdSetScissor(curr_command_buffer, 0, 1, &vulkan_instance.screen_stats.scissor);
				vkCmdSetViewport(curr_command_buffer, 0, 1, &vulkan_instance.screen_stats.viewport);

				vkCmdBeginRenderPass(curr_command_buffer, &renderpass_info, VK_SUBPASS_CONTENTS_INLINE);
				//for (auto&& camera : cameras)
				{
					auto& pipeline = pipelines[0];
					//for (auto&& pipeline : pipelines)
					{
						vkCmdBindPipeline(curr_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.pipeline.graphics_pipeline);
						for (auto&& model_weak : pipeline.models)
						{
							//for(size_t i = 0; i < 64000; ++i)
							if(auto model = model_weak.lock())
								model->draw(curr_command_buffer, pipeline.pipeline.pipeline_layout);
						}
					}
				}
				gui_manager.draw(curr_command_buffer);
				vkCmdEndRenderPass(curr_command_buffer);
				CHECK_VK_RESULT(vkEndCommandBuffer(curr_command_buffer));
			}
		}
		void draw_frame()
		{
			uint32_t image_index;
			VkResult result = vkAcquireNextImageKHR(vulkan_instance.logical_device, vulkan_instance.swapchain.swapchain, std::numeric_limits<uint64_t>::max(), vulkan_instance.image_available_semaphore, VK_NULL_HANDLE, &image_index);
			if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
			{
				recreate_swapchain();
				return;
			}
			CHECK_VK_RESULT(result);

			VkPipelineStageFlags shadow_map_wait_stages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			//begining render order
			VkSubmitInfo shadow_submit_info{};
			shadow_submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

			shadow_submit_info.waitSemaphoreCount = 1;
			shadow_submit_info.pWaitSemaphores = &vulkan_instance.image_available_semaphore;

			shadow_submit_info.pWaitDstStageMask = &shadow_map_wait_stages;
			shadow_submit_info.signalSemaphoreCount = 1;
			shadow_submit_info.pSignalSemaphores = &vulkan_instance.shadow_finished_semaphore;
			shadow_submit_info.commandBufferCount = 1;
			shadow_submit_info.pCommandBuffers = &vulkan_instance.command_buffers[image_index + vulkan_instance.command_buffers.size() / 2];

			//after the shadows are supposed to be completed
			VkSubmitInfo screen_submit_info{};
			screen_submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

			VkSemaphore wait_semaphores[] = { vulkan_instance.shadow_finished_semaphore,   };
			VkPipelineStageFlags wait_stages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };

			screen_submit_info.waitSemaphoreCount = 1;
			screen_submit_info.pWaitSemaphores = &vulkan_instance.shadow_finished_semaphore;
			screen_submit_info.pWaitDstStageMask = wait_stages;

			screen_submit_info.commandBufferCount = 1;
			screen_submit_info.pCommandBuffers = &vulkan_instance.command_buffers[image_index];

			VkSemaphore signal_semaphores[] = { vulkan_instance.render_finished_semaphore };

			screen_submit_info.signalSemaphoreCount = 1;
			screen_submit_info.pSignalSemaphores = signal_semaphores;

			VkSubmitInfo submit_infos[]{ shadow_submit_info, screen_submit_info };
			auto err = vkQueueSubmit(vulkan_instance.queues.graphics, 2, submit_infos, VK_NULL_HANDLE);
			CHECK_VK_RESULT(err);

			VkSwapchainKHR swapchains[] = { vulkan_instance.swapchain.swapchain };
			VkPresentInfoKHR present_info{};
			present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

			present_info.waitSemaphoreCount = 1;
			present_info.pWaitSemaphores = signal_semaphores;

			present_info.swapchainCount = 1;
			present_info.pSwapchains = &vulkan_instance.swapchain.swapchain;
			present_info.pImageIndices = &image_index;

			//present_info.pResults			= nullptr;

			result = vkQueuePresentKHR(vulkan_instance.queues.present, &present_info);
			vkQueueWaitIdle(vulkan_instance.queues.present);
		}
		void update_buffers()
		{
			static auto start = std::chrono::high_resolution_clock::now();
			float time = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000'000.f;

			for (auto& camera : cameras)
			{
				if (auto cam = camera.lock())
				{
					cam->update_buffer(vulkan_instance);
				}
			}
			shadow_camera.lock()->update_buffer(vulkan_instance);

			for (auto& model_ptr : pipelines[0].models)
			{
				if (auto model = model_ptr.lock())
				{
					model->update_uniform_buffers();
				}
			}			
		}
		void recreate_swapchain()
		{
			vkDeviceWaitIdle(vulkan_instance.logical_device);
			cleanup_swapchain();

			vulkan_instance.create_swapchain();
			vulkan_instance.create_swapchain_image_views();
			vulkan_instance.create_depth_image();
			vulkan_instance.create_framebuffers(render_pass.render_pass);
			for (auto& camera : cameras)
			{
				if (auto cam = camera.lock())
				{
					cam->perspective[0][0] = cam->perspective[1][1] / (vulkan_instance.swapchain.extent.width / (float)vulkan_instance.swapchain.extent.height);
					cam->perspective[0][0] *= -1;
				}
			}
			user_screen_resize_fnc(vulkan_instance.swapchain.extent.width, vulkan_instance.swapchain.extent.height);
			update_commandbuffers();
		}
		void cleanup_swapchain()
		{
			vulkan_instance.depth_image = {};

			vulkan_instance.swapchain.framebuffers = {};

			vkFreeCommandBuffers(vulkan_instance.logical_device, vulkan_instance.command_pool, (uint32_t)vulkan_instance.command_buffers.size(), vulkan_instance.command_buffers.data());

			vulkan_instance.swapchain.image_views = {};
			vulkan_instance.swapchain.swapchain = {};

			//this depends on swapchain_extent being specified every single render draw
			//and this requires stencil to be set every single render draw...
			//if we can fucking figure out how that works
		}
		void clean_dead_models()
		{
			auto& models = pipelines[0].models;
			models.erase(std::remove_if(models.begin(), models.end(), [](auto& val) {return val.expired(); }), models.end());
		}
		template<typename callback>
		void main_loop(const callback& user_callback)
		{
			gui_manager.new_frame(false, vulkan_instance);
			gui_manager.end_frame();
			gui_manager.update_buffers(vulkan_instance);
			
			update_commandbuffers();
			end = std::chrono::high_resolution_clock::now();
			while (!glfwWindowShouldClose(vulkan_instance.window))
			{
				vk_allocator->empty_garbage();
				clean_dead_models();
				start = std::chrono::high_resolution_clock::now();
				
				glfwPollEvents();

				//gui_manager.update_mouse_cursor();
				gui_manager.update_mouse_pos_and_buttons();
				gui_manager.new_frame(false, vulkan_instance);

				user_callback();

				gui_manager.end_frame();
				bool new_gui_state = gui_manager.update_buffers(vulkan_instance);

				if (rerecord_commandbuffers || new_gui_state)
				{
					update_commandbuffers();
				}
				update_buffers();
				draw_frame();
				end = std::chrono::high_resolution_clock::now();

				auto time = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
				auto tmp = vulkan_instance.app_name + std::string(" frame_time: ") + std::to_string(time.count());

				glfwSetWindowTitle(vulkan_instance.window, tmp.c_str());

				
			}
			exit();
		}
		void exit()
		{
			vkDeviceWaitIdle(vulkan_instance.logical_device);
		}
		static void engine_recast_fnc(void* engine_ptr)
		{
			static_cast<engine*>(engine_ptr)->recreate_swapchain();
		}
		void create_compute()
		{
			VkDeviceSize pixel_count = vulkan_instance.window_height * vulkan_instance.window_width;
			VkDeviceSize compute_vertex_buffer_size = pixel_count * 3 * sizeof(float);
			VkDeviceSize compute_index_buffer_size	= pixel_count * 6 * sizeof(unsigned int);

			auto compute_vertex_buffer	= gpu_data_buffer::create_gpu_only_buffer(compute_vertex_buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, vulkan_instance);
			auto compute_index_buffer	= gpu_data_buffer::create_gpu_only_buffer(compute_index_buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, vulkan_instance);

			//so we need a descriptor set that takes	a 2dsampler of a shadow map
			//											a vec3 array
			//											a unsigned int array
			//											a block of 2 matricies for camera data.
		}
	};
}