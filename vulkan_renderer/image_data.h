#pragma once
#include <vulkan\vulkan.h>
#include <vector>

struct image_data
{
	//this will probably get moved later as its stupidly common when interfacing with C like apis.
	template<typename type>
	struct unique_custom_deleter
	{
		type data;
		std::function<void(type)> deleter;
		unique_custom_deleter() {}
		unique_custom_deleter(type data_, const std::function<void(type)>& deleter_) :
			data(data_),deleter(deleter_)
		{}
		unique_custom_deleter(const unique_custom_deleter&) = delete;
		unique_custom_deleter(unique_custom_deleter&&) = default;
		unique_custom_deleter& operator=(const unique_custom_deleter&) = delete;
		unique_custom_deleter& operator=(unique_custom_deleter&&) = default;
		~unique_custom_deleter()
		{
			if(deleter)
				deleter(data);
		}
	};
	int width, height, depth;
	uint32_t layer_count;
	uint32_t mip_levels;
	uint32_t array_layers;
	VkFormat vulkan_image_format;
	unique_custom_deleter<unsigned char*> texels;

	int block_size;

	enum image_format
	{
		invalid,
		R,
		RG,
		RGB,
		RGBA
	};
	static image_data create_image(const std::string& filename, image_format format, bool use_mipmaps)
	{
		image_data img_data{};
		int tex_channels;
		img_data.depth = 1;
		img_data.texels = { stbi_load(filename.c_str(), &img_data.width, &img_data.height, &tex_channels, get_required_channels(format)),
							{ [](unsigned char* ptr) {stbi_image_free(ptr); } } };
		img_data.vulkan_image_format = texture_format(format);
		img_data.array_layers = 1;
		img_data.mip_levels = !use_mipmaps ? 1 : (uint32_t)std::log2(std::max(img_data.width,img_data.height)) +1;
		img_data.block_size = format;
		return img_data;
	}
	static image_data create_image(unsigned char* image_texels, std::function<void(unsigned char*)> texel_deleter, glm::ivec3 image_dims, image_format format, bool use_mipmaps)
	{
		image_data img_data{};
		img_data.depth = image_dims.z;
		img_data.width = image_dims.x;
		img_data.height = image_dims.y;
		img_data.texels = { image_texels, texel_deleter };
		img_data.vulkan_image_format = texture_format(format);
		img_data.array_layers = 1;
		img_data.mip_levels = !use_mipmaps ? 1 : (uint32_t)std::log2(std::max(img_data.width, img_data.height)) + 1;
		img_data.block_size = format;
		return img_data;
	}
	size_t size() const
	{
		return width*height*depth*block_size*sizeof(char);
	}
	VkExtent3D extent() const
	{
		return { (uint32_t)width,(uint32_t)height,(uint32_t)depth };
	}
private:
	static int get_required_channels(image_format format)
	{
		switch (format)
		{
		case R:
			return STBI_grey;
			break;
		case RG:
			return STBI_grey_alpha;
			break;
		case RGB:
			return STBI_rgb;
			break;
		case RGBA:
			return STBI_rgb_alpha;
			break;
		case invalid:
			return 0;
		}
		return 0;
	}
	static VkFormat texture_format(image_format format)
	{
		switch (format)
		{
		case R:
			return VK_FORMAT_R8_UNORM;
			break;
		case RG:
			return VK_FORMAT_R8G8_UNORM;
			break;
		case RGB:
			return VK_FORMAT_R8G8B8_UNORM;
			break;
		case RGBA:
			return VK_FORMAT_R8G8B8A8_UNORM;
			break;
		case invalid:
			return VK_FORMAT_UNDEFINED;
		}
		return VK_FORMAT_UNDEFINED;
	}
};