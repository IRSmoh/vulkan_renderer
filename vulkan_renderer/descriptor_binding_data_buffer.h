#pragma once
#include <vulkan\vulkan.h>

struct descriptor_binding_layout_data
{
	uint32_t binding;
	VkDescriptorType descriptor_type;
	uint32_t type_stride;
	uint32_t descriptor_array_size;	//e.g. if we'd normally pass '1' bone then it'd only be 1 in size, but normally it'd be N where N is num of bones.

	template<typename type, uint32_t binding>
	static constexpr descriptor_binding_layout_data make_descriptor_layout(VkDescriptorType descriptor_type)
	{
		return { binding, descriptor_type, sizeof(type), 1};
	}
	template<typename type>
	static constexpr descriptor_binding_layout_data make_descriptor_layout(uint32_t binding, VkDescriptorType descriptor_type)
	{
		return { binding, descriptor_type, sizeof(type),1};
	}
	template<typename type>
	static constexpr descriptor_binding_layout_data make_descriptor_layout(type&&, uint32_t binding, VkDescriptorType descriptor_type)
	{
		return { binding, descriptor_type, sizeof(type), 1};
	}
};
struct descriptor_binding_data_buffer
{
	descriptor_binding_layout_data layout;
	VkBuffer buffer;

	static descriptor_binding_data_buffer make_descriptor_buffer(descriptor_binding_layout_data layout, VkBuffer buffer)
	{
		return { layout, buffer };
	}
};