#pragma once
#include <vector>
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <imgui.h>
#include <misc/cpp/imgui_stdlib.h>
#include "shader_module.h"
#include "descriptor_layout.h"
#include "renderpass.h"
#include "pipeline.h"


namespace spawnx_vulkan
{
	struct gui_vertex
	{
		static std::vector<VkVertexInputBindingDescription> get_binding_description()
		{
			VkVertexInputBindingDescription descript{};
			descript.binding = 0;
			descript.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
			descript.stride = sizeof(ImDrawVert);
			return { descript };
		}
		static std::vector<VkVertexInputAttributeDescription> get_attribute_descriptions()
		{
			VkVertexInputAttributeDescription pos_attrib{};
			pos_attrib.binding = 0;
			pos_attrib.format = VK_FORMAT_R32G32_SFLOAT;
			pos_attrib.location = 0; //this is the part getting updated since we only use one binding slot
			pos_attrib.offset = offsetof(ImDrawVert, pos);

			VkVertexInputAttributeDescription uv_attrib{};
			uv_attrib.binding = 0;
			uv_attrib.format = VK_FORMAT_R32G32_SFLOAT;
			uv_attrib.location = 1; //this is the part getting updated since we only use one binding slot
			uv_attrib.offset = offsetof(ImDrawVert, uv);

			VkVertexInputAttributeDescription col_attrib{};
			col_attrib.binding = 0;
			col_attrib.format = VK_FORMAT_R8G8B8A8_UNORM;
			col_attrib.location = 2; //this is the part getting updated since we only use one binding slot
			col_attrib.offset = offsetof(ImDrawVert, col);

			return {pos_attrib, uv_attrib, col_attrib};

		}
	};
	class imgui_manager
	{
		bool render_gui;
	
		sampler_id font_sampler;
		image_id font_image;
		gpu_mesh text_mesh;

		pipeline gui_pipeline;
		descriptor_set_layout desc_layout;
		descriptor_set desc_set;

		static inline GLFWwindow* glfw_window;

		static inline std::array<GLFWcursor*, 8> mouse_cursors;
		static inline std::array<bool, 5> mouse_just_pressed{ false };

		static inline GLFWmousebuttonfun   prev_user_callback_mouse_button	= nullptr;
		static inline GLFWscrollfun        prev_user_callback_scroll		= nullptr;
		static inline GLFWkeyfun           prev_user_callback_key			= nullptr;
		static inline GLFWcharfun          prev_user_callback_char			= nullptr;

		constexpr static int shader_sampler_binding_id = 0;
	public:
		struct push_constant_block 
		{
			glm::vec2 scale;
			glm::vec2 translate;
		};
		push_constant_block push_constants;
		
		imgui_manager()
		{
			render_gui = true;
		}

		void gui_init(glm::vec2 display_dims)
		{
			ImGui::CreateContext();
			ImGuiStyle& style = ImGui::GetStyle();
			style.Colors[ImGuiCol_TitleBg]			= ImVec4(0.5f, 0.5f, 1.0f, 1.f);
			style.Colors[ImGuiCol_TitleBgActive]	= ImVec4(1.0f, 0.0f, 0.0f, 0.8f);
			style.Colors[ImGuiCol_MenuBarBg]		= ImVec4(1.0f, 0.0f, 0.0f, 0.4f);
			style.Colors[ImGuiCol_Header]			= ImVec4(1.0f, 0.0f, 0.0f, 0.4f);
			style.Colors[ImGuiCol_CheckMark]		= ImVec4(0.0f, 1.0f, 0.0f, 1.0f);

			ImGuiIO& io = ImGui::GetIO();
			io.DisplaySize = { display_dims.x, display_dims.y };
			io.DisplayFramebufferScale = { 1.f,1.f };
		}

		void init_resources(VkRenderPass render_pass, const std::string& font_vert_shader_filename, const std::string& font_frag_shader_filename, global_vulkan_instance_state& state)
		{
			ImGuiIO& io = ImGui::GetIO();

			//creat font's texture
			unsigned char* font_data;
			glm::ivec2 tex_dims;
			io.Fonts->GetTexDataAsRGBA32(&font_data, &tex_dims.x, &tex_dims.y);

			font_image = image_manager->create_image(font_data, [](auto) {}, "font texture", glm::ivec3(tex_dims, 1), image_data::RGBA, false, state);
			font_sampler = sampler_manager->create_sampler(texture_sampler::linear, state);
			
			//the shader vertex stage is fed by push constants.
			//the fragment gets a normal descriptor set/layout.
			//looks freaking odd not having vert descriptors passed...
			desc_layout = descriptor_set_layout::create_set_layout(
				{}, 
				{ descriptor_binding_layout_image::make_descriptor_layout(shader_sampler_binding_id,VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER) },
				state);

			desc_set = descriptor_set::create_descriptor_set(desc_layout, state);
			desc_set.write_descriptor_set({}, { {sampler_manager->get_sampler(font_sampler).sampler, image_manager->get_image(font_image).image_view} }, state);

			gui_pipeline = pipeline(state, VK_POLYGON_MODE_FILL);
			gui_pipeline.render_pass					= render_pass;
			gui_pipeline.shader_descriptor_set_layout	= desc_layout;

			auto pipeline_state = gui_pipeline.create_default_graphics_pipeline_state(true, VK_CULL_MODE_NONE, state);
			pipeline_state.color_blend_attachment.blendEnable			= VK_TRUE;
			pipeline_state.color_blend_attachment.colorWriteMask		= VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
			pipeline_state.color_blend_attachment.srcColorBlendFactor	= VK_BLEND_FACTOR_SRC_ALPHA;
			pipeline_state.color_blend_attachment.dstColorBlendFactor	= VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			pipeline_state.color_blend_attachment.colorBlendOp			= VK_BLEND_OP_ADD;
			pipeline_state.color_blend_attachment.srcAlphaBlendFactor	= VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			pipeline_state.color_blend_attachment.dstAlphaBlendFactor	= VK_BLEND_FACTOR_ZERO;
			pipeline_state.color_blend_attachment.alphaBlendOp			= VK_BLEND_OP_ADD;

			pipeline_state.color_blending					= {};
			pipeline_state.color_blending.sType				= VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			pipeline_state.color_blending.attachmentCount	= 1;
			pipeline_state.color_blending.pAttachments		= &pipeline_state.color_blend_attachment; //not needed as it'll just be set again when we pass it to create the pipeline.

			pipeline_state.depth_stencil_info.depthTestEnable = VK_FALSE;
			pipeline_state.depth_stencil_info.depthWriteEnable = VK_FALSE;

			VkPushConstantRange push_constant_range{};
			push_constant_range.offset		= 0;
			push_constant_range.size		= sizeof(push_constant_block);
			push_constant_range.stageFlags	= VK_SHADER_STAGE_VERTEX_BIT;

			pipeline_state.pipeline_layout_info.pushConstantRangeCount	= 1;
			pipeline_state.pipeline_layout_info.pPushConstantRanges		= &push_constant_range;

			//load shaders
			gui_pipeline.shader_modules.push_back(shader_module::load_vertex_shader(font_vert_shader_filename, state));
			gui_pipeline.shader_modules.push_back(shader_module::load_fragment_shader(font_frag_shader_filename, state));
			//make vertex attribs
			//create graphcis pipline and done.
			gui_pipeline.create_graphics_pipeline(std::move(pipeline_state), gui_vertex::get_binding_description(), gui_vertex::get_attribute_descriptions(), state);
		}

		void new_frame(bool update_frame_graph, global_vulkan_instance_state& state)
		{
			static float f = 0.f;
			static std::string str;
			ImGui::NewFrame();
			//yes?
			ImGui::TextUnformatted("some demo text to take up buffer space.");
			ImGui::Text("some other text");
			
			ImGui::Begin("asd##2");
			ImGui::Text("some text to make the label more obvious, also this window hides the one below it.");
			ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
			ImGui::InputText("pls gib text:", &str);
			ImGui::End();
		}
		void end_frame()
		{
			ImGui::Render();
		}
		//returns true if the buffers have been updated, meaning we need to issue a new draw for the entire scene... woo... fix this.
		bool update_buffers(global_vulkan_instance_state& state)
		{
			ImDrawData* im_draw_data = ImGui::GetDrawData();


			bool reupload_verts = text_mesh.gpu_buffers.empty() || text_mesh.gpu_buffers[0].size() != sizeof(ImDrawVert)*im_draw_data->TotalVtxCount;
			bool reupload_inds = text_mesh.indices.buffer() == VK_NULL_HANDLE || text_mesh.indices.size() != sizeof(ImDrawIdx)*im_draw_data->TotalIdxCount;
			//if (!reupload_inds && !reupload_verts)
			//	return false;

			
			text_mesh = {};
			text_mesh.index_type = VK_INDEX_TYPE_UINT16;

			if (im_draw_data->TotalIdxCount == 0)
			{
				render_gui = false;
				return true;
			}
			render_gui = true;

			auto vertex_copy_lam = [im_draw_data](void* dst_buff, auto ignored)
			{
				ImDrawVert* dst = (ImDrawVert*)(dst_buff);
				for (int i = 0; i < im_draw_data->CmdListsCount; ++i)
				{
					auto cmd_list = im_draw_data->CmdLists[i];
					memcpy(dst, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
					dst += cmd_list->VtxBuffer.Size;
				}
			};
			auto index_copy_lam = [im_draw_data](void* dst_buff, auto ignored)
			{
				ImDrawIdx* dst = (ImDrawIdx*)(dst_buff);
				for (int i = 0; i < im_draw_data->CmdListsCount; ++i)
				{
					auto cmd_list = im_draw_data->CmdLists[i];
					memcpy(dst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
					dst += cmd_list->IdxBuffer.Size;
				}
			};
			//we can safely ignore actually passing in a buffer since the lamba going to handle that for us.
			text_mesh.gpu_buffers.push_back(mesh_data_buffer::upload(nullptr, sizeof(ImDrawVert)*(size_t)im_draw_data->TotalVtxCount, vertex_copy_lam, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, state));
			//text_mesh.indices = mesh_data_buffer::upload(indices, im_draw_data->TotalIdxCount, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, state);
			text_mesh.indices = mesh_data_buffer::upload(nullptr, sizeof(ImDrawIdx)*(size_t)im_draw_data->TotalIdxCount, index_copy_lam, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, state);

			return true;
		}
		void draw(VkCommandBuffer command_buffer)
		{
			if (!render_gui)
				return;

			ImGuiIO& io = ImGui::GetIO();
			vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, gui_pipeline.pipeline_layout, 0, 1, &desc_set.set, 0, nullptr);
			vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, gui_pipeline.graphics_pipeline);

			text_mesh.bind(command_buffer, gui_pipeline.pipeline_layout, desc_set.set, (uint32_t)text_mesh.gpu_buffers.size());

			VkViewport viewport{};
			viewport.height		= io.DisplaySize.y;
			viewport.width		= io.DisplaySize.x;
			viewport.minDepth	= 0.f;
			viewport.maxDepth	= 1.f;

			vkCmdSetViewport(command_buffer, 0, 1, &viewport);

			push_constants.scale = glm::vec2(2.f / io.DisplaySize.x, 2.f / io.DisplaySize.y);
			push_constants.translate = glm::vec2(-1.f);

			vkCmdPushConstants(command_buffer, gui_pipeline.pipeline_layout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(push_constants), &push_constants);

			ImDrawData* im_draw_data = ImGui::GetDrawData();

			int32_t vertex_offset = 0;
			int32_t index_offset = 0;

			for (int32_t i = 0; i < im_draw_data->CmdListsCount; ++i)
			{
				ImDrawList* cmd_list = im_draw_data->CmdLists[i];
				for (int32_t j = 0; j < cmd_list->CmdBuffer.Size; ++j)
				{
					ImDrawCmd* draw_cmd = &cmd_list->CmdBuffer[j];

					VkRect2D scissor_rect{};

					scissor_rect.offset.x = std::max((int32_t)(draw_cmd->ClipRect.x), 0);
					scissor_rect.offset.y = std::max((int32_t)(draw_cmd->ClipRect.y), 0);

					scissor_rect.extent.width = (uint32_t)(draw_cmd->ClipRect.z - draw_cmd->ClipRect.x);
					scissor_rect.extent.height = (uint32_t)(draw_cmd->ClipRect.w - draw_cmd->ClipRect.y);

					vkCmdSetScissor(command_buffer, 0, 1, &scissor_rect);
					vkCmdDrawIndexed(command_buffer, draw_cmd->ElemCount, 1, index_offset, vertex_offset, 0);

					index_offset += draw_cmd->ElemCount;
				}
				vertex_offset += cmd_list->VtxBuffer.Size;
			}
		}
		//whole batch of functions basically uh, borrowed. yeah.
		//from imgui_impl_glfw.cpp.

		void setup_imgui_physical_inputs(GLFWwindow* window)
		{
			
			glfw_window = window;

			ImGuiIO& io = ImGui::GetIO();
			io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;         // We can honor GetMouseCursor() values (optional)
			io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;          // We can honor io.WantSetMousePos requests (optional, rarely used)
			io.BackendPlatformName = "imgui_glfw";

			// Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array.
			io.KeyMap[ImGuiKey_Tab]			= GLFW_KEY_TAB;
			io.KeyMap[ImGuiKey_LeftArrow]	= GLFW_KEY_LEFT;
			io.KeyMap[ImGuiKey_RightArrow]	= GLFW_KEY_RIGHT;
			io.KeyMap[ImGuiKey_UpArrow]		= GLFW_KEY_UP;
			io.KeyMap[ImGuiKey_DownArrow]	= GLFW_KEY_DOWN;
			io.KeyMap[ImGuiKey_PageUp]		= GLFW_KEY_PAGE_UP;
			io.KeyMap[ImGuiKey_PageDown]	= GLFW_KEY_PAGE_DOWN;
			io.KeyMap[ImGuiKey_Home]		= GLFW_KEY_HOME;
			io.KeyMap[ImGuiKey_End]			= GLFW_KEY_END;
			io.KeyMap[ImGuiKey_Insert]		= GLFW_KEY_INSERT;
			io.KeyMap[ImGuiKey_Delete]		= GLFW_KEY_DELETE;
			io.KeyMap[ImGuiKey_Backspace]	= GLFW_KEY_BACKSPACE;
			io.KeyMap[ImGuiKey_Space]		= GLFW_KEY_SPACE;
			io.KeyMap[ImGuiKey_Enter]		= GLFW_KEY_ENTER;
			io.KeyMap[ImGuiKey_Escape]		= GLFW_KEY_ESCAPE;
			io.KeyMap[ImGuiKey_KeyPadEnter] = GLFW_KEY_KP_ENTER;
			io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
			io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
			io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
			io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
			io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
			io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

			io.SetClipboardTextFn	= ImGui_Glfw_SetClipboardText;
			io.GetClipboardTextFn	= ImGui_Glfw_GetClipboardText;
			io.ClipboardUserData	= glfw_window;


			mouse_cursors[ImGuiMouseCursor_Arrow]		= glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
			mouse_cursors[ImGuiMouseCursor_TextInput]	= glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
			mouse_cursors[ImGuiMouseCursor_ResizeAll]	= glfwCreateStandardCursor(GLFW_ARROW_CURSOR);   // FIXME: GLFW doesn't have this.
			mouse_cursors[ImGuiMouseCursor_ResizeNS]	= glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
			mouse_cursors[ImGuiMouseCursor_ResizeEW]	= glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
			mouse_cursors[ImGuiMouseCursor_ResizeNESW]	= glfwCreateStandardCursor(GLFW_ARROW_CURSOR);  // FIXME: GLFW doesn't have this.
			mouse_cursors[ImGuiMouseCursor_ResizeNWSE]	= glfwCreateStandardCursor(GLFW_ARROW_CURSOR);  // FIXME: GLFW doesn't have this.
			mouse_cursors[ImGuiMouseCursor_Hand]		= glfwCreateStandardCursor(GLFW_HAND_CURSOR);

			prev_user_callback_mouse_button = glfwSetMouseButtonCallback(window,	ImGui_ImplGlfw_MouseButtonCallback);
			prev_user_callback_scroll		= glfwSetScrollCallback(window,			ImGui_ImplGlfw_ScrollCallback);
			prev_user_callback_key			= glfwSetKeyCallback(window,			ImGui_ImplGlfw_KeyCallback);
			prev_user_callback_char			= glfwSetCharCallback(window,			ImGui_ImplGlfw_CharCallback);

		}
		static const char* ImGui_Glfw_GetClipboardText(void* user_data)
		{
			return glfwGetClipboardString((GLFWwindow*)user_data);
		}

		static void ImGui_Glfw_SetClipboardText(void* user_data, const char* text)
		{
			glfwSetClipboardString((GLFWwindow*)user_data, text);
		}

		static void ImGui_ImplGlfw_MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
		{
			if (prev_user_callback_mouse_button != nullptr)
				prev_user_callback_mouse_button(window, button, action, mods);

			if (action == GLFW_PRESS && button >= 0 && button < (int)mouse_just_pressed.size())
				mouse_just_pressed[button] = true;
		}

		static void ImGui_ImplGlfw_ScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
		{
			if (prev_user_callback_scroll != nullptr)
				prev_user_callback_scroll(window, xoffset, yoffset);

			ImGuiIO& io = ImGui::GetIO();
			io.MouseWheelH += (float)xoffset;
			io.MouseWheel += (float)yoffset;
		}

		static void ImGui_ImplGlfw_KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
		{
			if (prev_user_callback_key != nullptr)
				prev_user_callback_key(window, key, scancode, action, mods);

			ImGuiIO& io = ImGui::GetIO();
			if (action == GLFW_PRESS)
				io.KeysDown[key] = true;
			if (action == GLFW_RELEASE)
				io.KeysDown[key] = false;

			// Modifiers are not reliable across systems
			io.KeyCtrl	= io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
			io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
			io.KeyAlt	= io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
			io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];
		}

		static void ImGui_ImplGlfw_CharCallback(GLFWwindow* window, unsigned int c)
		{
			if (prev_user_callback_char != nullptr)
				prev_user_callback_char(window, c);

			ImGuiIO& io = ImGui::GetIO();
			io.AddInputCharacter(c);
		}

		void update_mouse_cursor()
		{
			ImGuiIO& io = ImGui::GetIO();
			if ((io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange) || glfwGetInputMode(glfw_window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED)
				return;

			ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
			if (imgui_cursor == ImGuiMouseCursor_None || io.MouseDrawCursor)
			{
				// Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
				//glfwSetInputMode(glfw_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
			}
			else
			{
				// Show OS mouse cursor
				// FIXME-PLATFORM: Unfocused windows seems to fail changing the mouse cursor with GLFW 3.2, but 3.3 works here.
				glfwSetCursor(glfw_window, mouse_cursors[imgui_cursor] ? mouse_cursors[imgui_cursor] : mouse_cursors[ImGuiMouseCursor_Arrow]);
				//glfwSetInputMode(glfw_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			}
		}

		void update_mouse_pos_and_buttons()
		{
			ImGuiIO& io = ImGui::GetIO();
			for (int i = 0; i < IM_ARRAYSIZE(io.MouseDown); ++i)
			{
				io.MouseDown[i] = mouse_just_pressed[i] || glfwGetMouseButton(glfw_window, i) != 0;
				mouse_just_pressed[i] = false;
			}
			ImVec2 mouse_pos_backup = io.MousePos;
			io.MousePos = ImVec2(std::numeric_limits<float>::min(), std::numeric_limits<float>::min());

			bool focused = glfwGetWindowAttrib(glfw_window, GLFW_FOCUSED) != 0;

			if (focused)
			{
				if (io.WantSetMousePos)
				{
					glfwSetCursorPos(glfw_window, (double)mouse_pos_backup.x, (double)mouse_pos_backup.y);
				}
				else
				{
					double mouse_x, mouse_y;
					glfwGetCursorPos(glfw_window, &mouse_x, &mouse_y);
					io.MousePos = ImVec2((float)mouse_x, (float)mouse_y);
				}
			}
		}
	};
}