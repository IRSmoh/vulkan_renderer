#pragma once
#include <memory>
#include <vector>
#include <algorithm>
#include <tuple>
#include <type_traits>
#include "meta_templates.h"

//if you're compiling this on clang w/ microsoft code gen 3.8 or lower (or higher who knows) you MUST disable "Debug Information Format" set it to anything besides full debug information.
//release mode typically has line only information.
//the property is in >Property Pages>Config Properties>C/C++>General>Debug Information Format
//the crash that will result otherwise is DllGetC2Telemetry() + offsets. Good luck finding out the actual cause; I've tried.. there's no articles on google.
//tons of reports on visual studios ticket system; but no actual solutions yet just 'its fixed in visual studio "15" (it's not fixed)
//so yeah' no issues compiling in release for any version; debug only works on normal visual studio compilers until microsoft fixes their hack jobs.
//Update: works fine on Visual Studio 2017 Preview. (build tools v141) for VS and Clang.

namespace spawnx
{
	//use this to tell a delegate to be destroyed.
	class del_destruct_flag
	{
		std::weak_ptr<bool> dest_flag;
	public:
		del_destruct_flag(std::weak_ptr<bool> flag) : dest_flag(flag)
		{}
		del_destruct_flag()
		{}
		del_destruct_flag(const del_destruct_flag&) = delete;
		del_destruct_flag(del_destruct_flag&& right) = default;
		del_destruct_flag& operator=(const del_destruct_flag&) = delete;
		del_destruct_flag& operator=(del_destruct_flag&&) = default;
		~del_destruct_flag()
		{
			*this = true;	//auto flag the delegate to be destroyed, since the only way this can be called is the owning class/block to be destroyed/scope exited.
		}
		explicit operator bool() const
		{
			auto flag = dest_flag.lock();
			return flag ? *flag : false;
		}
		void operator=(bool right)
		{
			if (auto flag = dest_flag.lock())
			{
				*flag = right;
			}
		}
		bool empty() const
		{
			if (auto flag = dest_flag.lock())
			{
				return flag.get() == nullptr;
			}
			return true;
		}
	};
	//because its far, far easier to inherit from a class where the destruct flags are auto managed
	struct flag_wrapper
	{
		std::vector<del_destruct_flag> flags = {};
		void add_flag(del_destruct_flag&& flag)
		{
			flags.push_back(std::move(flag));
		}
	};
	//a more lite version of the above class; so we can pack normal function pointers more dense.
	template<typename return_type, typename... args>
	class delegate_callback_function
	{
		using callback = return_type(*)(args...);
		callback func;
	public:
		delegate_callback_function(callback _func) : func(_func) {}

		return_type operator()(args&... params) const
		{
			return (*func)(params...);
		}
		bool operator==(const delegate_callback_function& right) const
		{
			return func == right.func;
		}
		bool operator!=(const delegate_callback_function& right) const
		{
			return !(*this == right);
		}
	};
	template<typename return_type, typename... args>
	class delegate_functor_base
	{
	public:
		virtual return_type operator()(args&... params) = 0;
	};
	//functor must be a class that implements ::operator()(args...);
	template<typename functor, typename return_type, typename... args>
	class delegate_functor : public delegate_functor_base<return_type, args...>
	{
		functor callable;
	public:
		delegate_functor(functor&& _callable) : callable(std::move(_callable)) {}
		return_type operator()(args&... params) override final
		{
			return callable(params...);
		}
	};
	template<typename return_type, typename... args>
	class delegate_with_dest_flag_base
	{
	public:
		std::shared_ptr<bool> dest_flag;
		virtual return_type operator()(args&... params) = 0;
	};
	template<typename functor, typename return_type, typename... args>
	class delegate_with_dest_flag : public delegate_with_dest_flag_base<return_type, args...>
	{
		functor callable;
	public:
		delegate_with_dest_flag(functor&& _callable, std::shared_ptr<bool> _dest_flag) : callable(std::move(_callable))
		{
			this->dest_flag = _dest_flag;
		}
		return_type operator()(args&... params) override final
		{
			return callable(params...);
		}
	};
	template<typename return_type, typename ... args>
	class delegate
	{
		std::vector<std::unique_ptr<delegate_with_dest_flag_base<return_type, args...>>>	delegates;
		std::vector<std::unique_ptr<delegate_functor_base<return_type, args...>>>			delegates_functors;
		std::vector<delegate_callback_function<return_type, args...>>						delegates_functions;

		template<typename ret_type, std::enable_if_t<std::is_void<ret_type>::value, bool> = true>
		void call_impl(args&... params)
		{
			//run normal free-standing functions.
			for (auto& function : delegates_functions)
			{
				function(params...);
			}
			//run all lambdas/functionals/binds/etc
			for (auto& functor : delegates_functors)
			{
				(*functor)(params...);
			}
			//run functions that could be removed; e.g. all classes and free-standing functions that the user requested a delegate_del_flag for.
			auto parse_lam = [&params...](auto& del)
			{
				if (*(*del).dest_flag)
				{
					return true;
				}
				(*del)(params...);
				return false;
			};
			delegates.erase(std::remove_if(delegates.begin(), delegates.end(), parse_lam), delegates.end());
		}
		template<typename ret_type, std::enable_if_t<!std::is_void<ret_type>::value, bool> = true>
		std::vector<return_type> call_impl(args&... params)
		{
			std::vector<return_type> returns;
			returns.reserve(delegates_functions.size() + delegates.size() + delegates_functors.size());
			//run normal free-standing functions.
			for (auto& function : delegates_functions)
			{
				returns.push_back(function(params...));
			}
			//run lambdas/functions/etc
			for (auto& functor : delegates_functors)
			{
				returns.push_back((*functor)(params...));
			}
			//run functions that could be removed; e.g. member functions/free-standing functions that the user requested a delegate_del_flag.
			auto parse_lam = [&params..., &returns](auto& del)
			{
				if (*(*del).dest_flag)
				{
					return true;
				}
				returns.push_back((*del)(params...));
				return false;
			};
			delegates.erase(std::remove_if(delegates.begin(), delegates.end(), parse_lam), delegates.end());
			return returns;
		}
		template<typename ret_type, typename arg, std::enable_if_t<std::is_void<ret_type>::value, bool> = true>
		void call_impl(std::vector<arg>& params)
		{
			//run normal free-standing functions.
			for (auto& function : delegates_functions)
			{
				for (auto& param : params)
				{
					function(param);
				}
			}
			//run all lambdas/functionals/binds/etc
			for (auto& functor : delegates_functors)
			{
				for (auto& param : params)
				{
					(*functor)(param);
				}
			}
			//run functions that could be removed; e.g. all classes and free-standing functions that the user requested a delegate_del_flag for.
			auto parse_lam = [&params](auto& del)
			{
				if (*(*del).dest_flag)
				{
					return true;
				}
				for (auto& param : params)
				{
					(*del)(param);
				}
				return false;
			};
			delegates.erase(std::remove_if(delegates.begin(), delegates.end(), parse_lam), delegates.end());
		}
		template<typename ret_type, typename arg, std::enable_if_t<!std::is_void<ret_type>::value, bool> = true>
		std::vector<return_type> call_impl(std::vector<arg>& params)
		{
			std::vector<return_type> returns;
			returns.reserve(delegates_functions.size() + delegates.size() + delegates_functors.size());
			//run normal free-standing functions.
			for (auto& function : delegates_functions)
			{
				for (auto& param : params)
				{
					returns.push_back(function(param));
				}
			}
			//run all lambdas/functionals/binds/etc
			for (auto& functor : delegates_functors)
			{
				for (auto& param : params)
				{
					returns.push_back((*functor)(param));
				}
			}
			//run functions that could be removed; e.g. all classes and free-standing functions that the user requested a delegate_del_flag for.
			auto parse_lam = [&returns, &params](auto& del)
			{
				if (*(*del).dest_flag)
				{
					return true;
				}
				for (auto& param : params)
				{
					returns.push_back((*del)(param));
				}
				return false;
			};
			delegates.erase(std::remove_if(delegates.begin(), delegates.end(), parse_lam), delegates.end());
			return returns;
		}
		template<typename ret_type, typename... _args, size_t... indices, std::enable_if_t<std::is_void<ret_type>::value, bool> = true>
		void call_impl(std::vector<std::tuple<_args...>>& params, std::index_sequence<indices...>)
		{
			//run normal free-standing functions.
			for (auto& function : delegates_functions)
			{
				for (auto& param : params)
				{
					function(std::get<indices>(param)...);
				}
			}
			//run all lambdas/functionals/binds/etc
			for (auto& functor : delegates_functors)
			{
				for (auto& param : params)
				{
					(*functor)(std::get<indices>(param)...);
				}
			}
			//run functions that could be removed; e.g. all classes and free-standing functions that the user requested a delegate_del_flag for.
			auto parse_lam = [&params](std::unique_ptr<delegate_with_dest_flag_base<return_type, args...>>& del)
			{
				if (*(*del).dest_flag)
				{
					return true;
				}
				for (auto& param : params)
				{
					(*del)(std::get<indices>(param)...);
				}
				return false;
			};
			delegates.erase(std::remove_if(delegates.begin(), delegates.end(), parse_lam), delegates.end());
		}
		template<typename ret_type, typename... _args, size_t... indices, std::enable_if_t<!std::is_void<ret_type>::value, bool> = true>
		std::vector<ret_type> call_impl(std::vector<std::tuple<_args...>>& params, std::index_sequence<indices...>)
		{
			std::vector<ret_type> returns;
			returns.reserve(delegates_functions.size() + delegates.size() + delegates_functors.size());
			//run normal free-standing functions.
			for (auto& function : delegates_functions)
			{
				for (auto& param : params)
				{
					returns.push_back(function(std::get<indices>(param)...));
				}
			}
			//run all lambdas/functionals/binds/etc
			for (auto& functor : delegates_functors)
			{
				for (auto& param : params)
				{
					returns.push_back((*functor)(std::get<indices>(param)...));
				}
			}
			//run functions that could be removed; e.g. all classes and free-standing functions that the user requested a delegate_del_flag for.
			auto parse_lam = [&returns, &params](std::unique_ptr<delegate_with_dest_flag_base<return_type, args...>>& del)
			{
				if (*(*del).dest_flag)
				{
					return true;
				}
				for (auto& param : params)
				{
					returns.push_back((*del)(std::get<indices>(param)...));
				}
				return false;
			};
			delegates.erase(std::remove_if(delegates.begin(), delegates.end(), parse_lam), delegates.end());
			return returns;
		}

		template<typename lambda, typename ... args>
		using lambda_mem_fnc = std::disjunction<has_lambda_mem_fnc<lambda>, has_auto_lambda_mem_fnc<lambda, args...>>;

		template<typename lambda_mem_fnc, std::enable_if_t<std::conjunction<
			std::negation<std::is_same<std::false_type, lambda_mem_fnc>>,
			is_more_restrictive<std::tuple<args...>, typename func_arg_solver_t<lambda_mem_fnc>::args>
		>::value, bool> = true>
			std::true_type lambda_assert_helper()
		{
			return std::true_type();
		}
		template<typename lambda_mem_fnc, std::enable_if_t<std::conjunction<
			std::negation<std::is_same<std::false_type, lambda_mem_fnc>>,
			std::negation<is_more_restrictive<std::tuple<args...>, typename func_arg_solver_t<lambda_mem_fnc>::args>>
		>::value, bool> = true>
			std::false_type lambda_assert_helper()
		{
			return std::false_type();
		}
		template<typename lambda_mem_fnc, std::enable_if_t<std::is_same<std::false_type, lambda_mem_fnc>::value, bool> = true>
		std::true_type lambda_assert_helper()
		{
			return std::true_type();
		}
		template<typename lam, std::enable_if_t<!std::is_pointer<lam>::value, bool> = false>
		void add_uncastable_or_capture_lambda(lam lambda)
		{
			delegates_functors.push_back(std::make_unique<delegate_functor<lam, return_type, args...>>(delegate_functor<lam, return_type, args...>(std::move(lambda))));
		}
	public:
		delegate() {}
		//no, this does not *have* to be a move only class
		//but it does not make a ton of sense to be duplicating the delegates and their callbacks around.
		//so if for some reason you need to move them.. well. std::move them.
		delegate(const delegate&) = delete;
		delegate& operator=(const delegate&) = delete;
		delegate(delegate&&) = default;
		delegate& operator=(delegate&&) = default;

		decltype(auto) operator()(args... params)
		{
			return call_impl<return_type>(params...);
		}
		//I'd love to make the container access const.. but that forces all the data members to be const also (duh now that I think about it)
		//but we can't guarantee that's what all delegates want.. I mean its reasonable typically but not guaranteed.
		//so this is a 'comment' promise. the supplied container will not have elements added/deleted; we're just accessing the elements.
		template<typename _arg, std::enable_if_t<is_more_restrictive<std::tuple<_arg>, std::tuple<std::tuple_element_t<0, std::tuple<args...>>>>::value, bool> = true>
		decltype(auto) operator()(std::vector<_arg>& params)
		{
			return call_impl<return_type, args...>(params);
		}
		template<typename... _args, std::enable_if_t<is_more_restrictive<std::tuple<_args...>, std::tuple<args...>>::value, bool> = true>
		decltype(auto) operator()(std::vector<std::tuple<_args...>>& params)
		{
			//we have to pass our params along with a supplied index_sequence so we can unpack them properly
			//std::get<type>() will fail due to how often types get reused in function arguments.
			//std::get<index>() will work fine as it never cares about the type in the first place.
			return call_impl<return_type>(params, std::index_sequence_for<args...>{});//delegate_impl<return_type, args...>::call_impl(delegates, delegates_functors, delegates_functions, params, std::index_sequence_for<_args...>());
		}
		//working grounds shit
		template<typename func, std::enable_if_t<std::is_pointer<func>::value, bool> = true>
		void add(func function)
		{
			using func_args = func_arg_solver_t<func>;
			static_assert(is_safe_to_func_cast<std::tuple<args...>, typename func_args::args>::value && std::is_same<return_type, typename func_args::ret>::value, "Function parameters {return,args} must match, or be more specialized than the delegates.");
			//since this is long for a single call.
			//we make sure that our function can accept the types we want to pass to it without UB that's the static check.
			//then since we know it won't cause UB anymore; we can safely reinterpret_cast<> it to our desired type.
			//so long as the cast is from a more restrictive function to a less restrictive it will work fine.
			delegates_functions.push_back(delegate_callback_function<return_type, args...>(reinterpret_cast<return_type(*)(args...)>(function)));
		}
		template<typename obj, typename method>
		void add(obj* _object, method _func)
		{
			using func_args = func_arg_solver_t<method>;
			static_assert(is_more_restrictive<std::tuple<args...>, typename func_args::args>::value && std::is_same<return_type, typename func_args::ret>::value, "Method parameters {return,args} must match, or be more specialized than the delegates.");
			static_assert(is_derived<obj, flag_wrapper>::value, "Auto managed objects must inherit from flag_wrapper. If you want to manage the del_destruct_flags yourself use ::add_self_managed instead.");
			auto lam = [object = _object, func = _func](args... params)
			{
				return (object->*func)(params...);
			};
			_object->add_flag(this->add_self_managed(lam));
		}

		//matches 1:1 with expected function
		//templated, but otherwise castable.
		template<typename lambda, typename mem_fnc = lambda_mem_fnc<lambda, args...>, typename func_info = func_arg_solver_t<typename mem_fnc::type>,
			std::enable_if_t<std::conjunction<
			std::negation<std::is_pointer<lambda>>,
			std::is_empty<lambda>,
			is_safe_to_func_cast<std::tuple<args...>, typename func_info::args>,
			mem_fnc
		>::value, bool> = true>
			void add(lambda lam)
		{
			//since in a week or two time this will read like black magic....
			//we build what a valid lambda casting target -> func_arg_solver_t::func_ptr_type
			//and then we take our lambda and cast it over via its func_ptr* operator aided by static_cast.
			//this way we can optimize some of the polymorphism away and have slightly less overhead.
			add(static_cast<typename func_info::func_ptr_type>(lam));
		}

		//matches with expected function but qualifiers dont aline (try to use as per typical ... no func cast)
		//templated stupidly because 'efficient' ([](const auto&){}) <<< bad.. just leave it as [](auto){} for the love of god. but we can *still* take it. (throw warning)
		template<typename lambda, typename mem_fnc = lambda_mem_fnc<lambda, args...>, typename func_info = func_arg_solver_t<typename mem_fnc::type>,
			std::enable_if_t<std::conjunction<
			std::negation<std::is_pointer<lambda>>,
			std::is_empty<lambda>,
			std::disjunction<
			std::negation<is_safe_to_func_cast<std::tuple<args...>, typename func_info::args>>,
			std::negation<mem_fnc>>
			>::value, bool> = true>
			void add(lambda lam)
		{
			//even though its not safe to cast the lambda.. we can probably still use it (well hopefully people aren't so thick to pass lambdas to us..)
			//so instead embed it as a non empty lambda.
			static_assert(decltype(lambda_assert_helper<typename mem_fnc::type>())::value, "Lambda parameters {return, args} must match or be more specialized than the delegates.");
			add_uncastable_or_capture_lambda(lam);
		}

		template<typename lambda, typename mem_fnc = lambda_mem_fnc<lambda, args...>,
			std::enable_if_t<std::conjunction<
			std::negation<std::is_pointer<lambda>>,
			std::negation<std::is_empty<lambda>>
		>::value, bool> = true>
			void add(lambda lam)
		{
			//the final fall through for all capturing lambdas.. since we can't cast them to a function pointer to handle more efficiently.. just store them directly.
			//this assert checks to see if the lambda's operator() can be determined. if it can we check to make sure its valid to call as a embedded function (e.g. ref->non ref works fine)
			//and normal qualifier rules apply..
			//if the operator cannot be determined we don't bother triggering the assert and instead assume the user provided us with something we just couldn't understand..
			//not something hilariously invalid. e.g. we expect {int} and you give us {int*}
			//but you could pass something like [](int x, auto y){...}; and since we expect 2 arguments its possibly valid; but theres no way to easily grab the operator().

			static_assert(decltype(lambda_assert_helper<typename mem_fnc::type>())::value, "Lambda parameters {return, args} must match or be more specialized than the delegates.");
			add_uncastable_or_capture_lambda(lam);
		}

		template<typename func, std::enable_if_t<std::is_pointer<func>::value, bool> = true>
		del_destruct_flag add_self_managed(func function)
		{
			using func_args = func_arg_solver_t<func>;
			static_assert(is_safe_to_func_cast<std::tuple<args...>, typename func_args::args>::value && std::is_same<return_type, typename func_args::ret>::value, "Function parameters {return,args} must match, or be more specialized than the delegates.");
			std::shared_ptr<bool> dest_flag = std::make_shared<bool>(false);
			auto lambda = [func_ptr = function](args&... params) {return (*func_ptr)(params...); };
			delegates.push_back(
				std::make_unique<delegate_with_dest_flag<decltype(lambda), return_type, args...>>(
					delegate_with_dest_flag<decltype(lambda), return_type, args...>(std::move(lambda), dest_flag)));
			return del_destruct_flag(dest_flag);
		}
		template<typename lam, std::enable_if_t<!std::is_pointer<lam>::value, bool> = false>
		del_destruct_flag add_self_managed(lam lambda)
		{
			//theres no optimizations to make since we've made all destruct_flags handled the same way (polymorphic base class)
			//otherwise the same assert rules apply in that we check for as many blockable cases as possible but assume the user is at least attempting to give us something valid.
			using mem_fnc = lambda_mem_fnc <lam, args... >;
			static_assert(decltype(lambda_assert_helper<typename mem_fnc::type>())::value, "Lambda parameters {return, args} must match or be more specialized than the delegates.");

			std::shared_ptr<bool> dest_flag = std::make_shared<bool>(false);
			delegates.push_back(std::make_unique<delegate_with_dest_flag<lam, return_type, args...>>(delegate_with_dest_flag<lam, return_type, args...>(std::move(lambda), dest_flag)));
			return del_destruct_flag(dest_flag);
		}
		template<typename obj, typename method>
		del_destruct_flag add_self_managed(obj* _object, method _func)
		{
			using func_args = func_arg_solver_t<method>;
			static_assert(is_more_restrictive<std::tuple<args...>, typename func_args::args>::value && std::is_same<return_type, typename func_args::ret>::value, "Method parameters {return,args} must match, or be more specialized than the delegates.");
			auto lam = [object = _object, func = _func](args... params)
			{
				return (object->*func)(params...);
			};
			return this->add_self_managed(lam);
		}
	};
}