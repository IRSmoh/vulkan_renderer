#version 450

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 frag_normal;
layout(location = 1) out vec2 fragTexCoord;
layout(location = 2) out vec4 light_space_pos;
layout(location = 3) out vec4 light_direction;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(binding = 0) uniform UniformBufferObject
{
	mat4 model;
} matrices;
layout (binding = 1) uniform camera_data
{
	mat4 view;
	mat4 proj;
} camera;
layout (binding = 2) uniform shadow_data
{
	mat4 view;
	mat4 proj;
} shadow_camera;

void main() 
{
	vec4 world_pos = matrices.model* vec4(inPosition, 1.0);
	
    gl_Position = camera.proj* camera.view * world_pos;
	
	light_space_pos = shadow_camera.proj* shadow_camera.view* world_pos;
	light_direction = shadow_camera.view*vec4(0.0,0.0,1.0,0.0);
	
    frag_normal = normalize(inNormal);
	fragTexCoord = inTexCoord;
}