#pragma once
#include "vulkan_renderer.h"
struct gpu_info
{
	VkPhysicalDeviceProperties properties;
	VkPhysicalDeviceMemoryProperties memory_properties;
	VkPhysicalDeviceFeatures features;
	VkSurfaceCapabilitiesKHR surface_capabilities;
};