#pragma once
#include "vulkan_renderer.h"
#include <utility>
namespace spawnx_vulkan
{
	template<typename type>
	struct data_and_buffer
	{
		std::vector<type> data;
		size_t elem_count;
		gpu_data_buffer gpu_buffer;
		template<typename global_vulkan_instance_state_type>
		void upload(VkBufferUsageFlags usage, const global_vulkan_instance_state_type& instance)
		{
			gpu_buffer = gpu_data_buffer::upload_data(data, usage, instance);
		}
	};
	struct mesh_data_buffer
	{
		gpu_data_buffer gpu_buffer;
		size_t elem_count;
		mesh_data_buffer() {}
		mesh_data_buffer(gpu_data_buffer&& buffer, size_t elem_c) : gpu_buffer(std::move(buffer)), elem_count(elem_c) {}
		mesh_data_buffer(mesh_data_buffer&& right) : gpu_buffer(std::move(right.gpu_buffer)), elem_count(right.elem_count) {}
		mesh_data_buffer& operator=(mesh_data_buffer&& right)
		{
			gpu_buffer = std::move(right.gpu_buffer);
			elem_count = right.elem_count;
			return *this;
		}
		template<typename data_type, typename global_vulkan_instance_state_type>
		static mesh_data_buffer upload(const std::vector<data_type>& data, VkBufferUsageFlags usage, const global_vulkan_instance_state_type& instance)
		{
			return {	gpu_data_buffer::upload_data(data, usage, instance),
						data.size() };
		}
		template<typename data_type, typename global_vulkan_instance_state_type>
		static mesh_data_buffer upload(const data_type* data, std::size_t elem_count, VkBufferUsageFlags usage, const global_vulkan_instance_state_type& instance)
		{
			return {	gpu_data_buffer::upload_data(data, elem_count, usage, instance),
						elem_count };
		}
		//callable is of the form: auto lam = [](copy_dst, copy_src){...};
		//data is directly fed to the calling lambda, if you intend to ignore it (aka the copy src) you can safely pass in a nullptr.
		//lambda can have whatever state needed.
		//this will be called with the staging buffer as the destination, some source, and will run before uploading the data to the gpu
		//basically its useful if you need to upload disjoint arrays/non contigous data that is mostly* in contiguous blocks.
		//e.g. arrays of arrays type**
		template<typename data_type, typename callable_type, typename global_vulkan_instance_state_type>
		static mesh_data_buffer upload(data_type data, std::size_t elem_count, const callable_type& callable, VkBufferUsageFlags usage, const global_vulkan_instance_state_type& instance)
		{
			return { gpu_data_buffer::upload_data(data, elem_count, callable, usage, instance),
						elem_count };
		}
		const VkBuffer* buffer_ptr() const
		{
			return &gpu_buffer.buffer;
		}
		VkBuffer buffer() const
		{
			return gpu_buffer.buffer;
		}
		size_t size() const
		{
			return elem_count;
		}
	};

	struct position
	{
		using type = glm::vec3;
		std::vector<type> data;
	};
	struct normal
	{
		using type = glm::vec3;
		std::vector<type> data;
	};
	struct uvs
	{
		using type = glm::vec2;
		std::vector<type> data;
	};
	template<typename ty>
	using dependent_type = typename ty::type;
	struct gpu_mesh
	{
		std::vector<mesh_data_buffer> gpu_buffers;
		mesh_data_buffer indices;
		VkIndexType index_type = VK_INDEX_TYPE_UINT32;
		std::array<VkDeviceSize, 1> offsets{ 0 };

		gpu_mesh() {}
		gpu_mesh(std::vector<mesh_data_buffer>&& init_list, mesh_data_buffer&& index_buffer) : gpu_buffers(std::move(init_list)), indices(std::move(index_buffer))
		{}

		void draw(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout, VkDescriptorSet descriptor_set)
		{
			draw_impl(command_buffer, pipeline_layout, descriptor_set, (uint32_t)gpu_buffers.size());
		}
		void draw_shadows(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout, VkDescriptorSet descriptor_set)
		{
			draw_impl(command_buffer, pipeline_layout, descriptor_set, 1);
		}
		//end_buffer_count allows us to reuse the same buffer(s).. basically set it to 
		void bind(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout, VkDescriptorSet descriptor_set, uint32_t end_buffer_count)
		{
			for (uint32_t curr_buffer = 0; curr_buffer < end_buffer_count; ++curr_buffer)
			{
				vkCmdBindVertexBuffers(command_buffer, curr_buffer, 1, gpu_buffers[curr_buffer].buffer_ptr(), offsets.data());
			}

			vkCmdBindIndexBuffer(command_buffer, indices.buffer(), 0, index_type);
			vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
		}
	private:

		void draw_impl(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout, VkDescriptorSet descriptor_set, uint32_t end_buffer_count)
		{
			bind(command_buffer, pipeline_layout, descriptor_set, end_buffer_count);
			vkCmdDrawIndexed(command_buffer, (uint32_t)indices.size(), 1, 0, 0, 0);
		}
	};
	struct cpu_mesh_offsets
	{
		size_t buffer_data_offset;
		size_t index_offset;
		cpu_mesh_offsets() : buffer_data_offset(0), index_offset(0) {}
		cpu_mesh_offsets(size_t buffer, size_t index) : buffer_data_offset(buffer), index_offset(index) {}
	};
	template<typename... vertex_buffer_types>
	struct cpu_mesh
	{
		std::tuple<vertex_buffer_types...> data;
		std::vector<std::uint32_t> indices;

		template<typename global_vulkan_instance_state_type>
		gpu_mesh upload(const global_vulkan_instance_state_type& vulkan_state) const
		{
			return upload_impl(vulkan_state, std::make_index_sequence<sizeof...(vertex_buffer_types)>{});
		}

		template<typename vertex_buffer_type>
		auto& get()
		{
			return std::get<vertex_buffer_type>(data).data;
		}
		std::vector<std::uint32_t>& get_indices()
		{
			return indices;
		}
		cpu_mesh_offsets get_curr_offsets() const
		{
			return { std::get<0>(data).data.size(), indices.size() };
		}
		void resize_mesh(const cpu_mesh_offsets& offsets)
		{
			indices.resize(offsets.index_offset);
			spawnx_vulkan::apply(data, [&](auto& vec) {vec.data.resize(offsets.buffer_data_offset); });
		}
	private:
		template<typename global_vulkan_instance_state_type, size_t ... inds>
		gpu_mesh upload_impl(const global_vulkan_instance_state_type& vulkan_state, std::index_sequence<inds...>) const
		{
			auto func = [&](auto& data_to_upload)
			{
				return mesh_data_buffer::upload(data_to_upload.data, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, vulkan_state);
			};
			std::vector<mesh_data_buffer> gpu_buffers;
			(gpu_buffers.push_back(func(std::get<inds>(data))), ...);
			return { std::move(gpu_buffers), mesh_data_buffer::upload(indices, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, vulkan_state) };
		}
	};

	using default_cpu_mesh = cpu_mesh<spawnx_vulkan::position, spawnx_vulkan::normal, spawnx_vulkan::uvs>;
}