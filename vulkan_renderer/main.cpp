#include "vulkan_renderer.h"
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\hash.hpp>
#include "command_buffer_data_holder.h"
#include <chrono>
#include "engine.h"
int main()
{
	{
		spawnx_vulkan::engine engine{};
		engine.startup(false);

		float view_distance = 1'000;
		std::shared_ptr<spawnx_vulkan::camera> cam = std::make_shared<spawnx_vulkan::camera>(spawnx_vulkan::camera::create_camera(glm::perspective(glm::radians(70.f), engine.vulkan_instance.window_width / (float)engine.vulkan_instance.window_height, 0.1f, view_distance), engine.vulkan_instance));
		std::shared_ptr<spawnx_vulkan::camera> shadow_cam = std::make_shared<spawnx_vulkan::camera>(spawnx_vulkan::camera::create_camera(glm::perspective(glm::radians(70.f), 1.f, 0.1f, view_distance), engine.vulkan_instance));

		engine.add_camera(cam);
		engine.add_shadow_camera(shadow_cam);

		engine.descriptor_set_creation();
		engine.main_loop([]() {});
	}
}