#pragma once
#include "vulkan_renderer.h"
namespace spawnx_vulkan
{
	using image_id = strong_type<std::size_t, struct image_id_tag>;
	struct vulkan_image_manager
	{
		struct image_and_name
		{
			vulkan_image image;
			std::string image_name;
		};
		std::vector<image_and_name> images;
		image_id create_image(const std::string& filename, image_data::image_format image_format, bool create_mipmaps, global_vulkan_instance_state& vulkan_state)
		{
			for (size_t i = 0; i < images.size(); ++i)
				if (images[i].image_name == filename)
					return{ i };
			images.push_back({ vulkan_image::create_image_2D(image_data::create_image(filename, image_format, create_mipmaps), vulkan_state),filename });
			return { images.size() - 1 };
		}
		image_id create_image(unsigned char* texel_data, std::function<void(unsigned char*)> texel_deleter, const std::string& name, glm::ivec3 image_dims, image_data::image_format image_format, bool create_mipmaps, global_vulkan_instance_state& vulkan_state)
		{
			for (size_t i = 0; i < images.size(); ++i)
				if (images[i].image_name == name)
					return{ i };
			
			images.push_back({ vulkan_image::create_image_2D(image_data::create_image(texel_data, texel_deleter, image_dims, image_format, create_mipmaps),vulkan_state) });
			return { images.size() - 1 };
		}
		image_id find_image(const std::string& filename) const
		{
			for (size_t ind = 0; ind < images.size(); ++ind)
				if (images[ind].image_name == filename)
					return { ind };
			return { (size_t)-1 };
		}
		const vulkan_image& get_image(image_id id) const
		{
			if (id > images.size())
				throw std::runtime_error("Attempting to access an out of bounds image");
			return images[id].image;
		}
	};
	using sampler_id = strong_type<std::size_t, struct sampler_id_tag>;
	struct vulkan_sampler_manager
	{
		struct sampler_and_filter
		{
			texture_sampler sampler;
			texture_sampler::filter filter;
		};
		std::vector<sampler_and_filter> samplers;
		sampler_id create_sampler(texture_sampler::filter filter, global_vulkan_instance_state& vulkan_state)
		{
			for (size_t i = 0; i < samplers.size();++i)
				if (samplers[i].filter == filter)
					return { i };
			samplers.push_back({ texture_sampler::create(filter,vulkan_state),filter });
			return { samplers.size() -1};
		}
		sampler_id create_clamped_sampler(texture_sampler::filter filter, global_vulkan_instance_state& vulkan_state)
		{
			for (size_t i = 0; i < samplers.size(); ++i)
				if (samplers[i].filter == filter)
					return { i };
			samplers.push_back({ texture_sampler::create_clamped(filter,vulkan_state),filter });
			return { samplers.size() - 1 };
		}
		sampler_id find_sampler(texture_sampler::filter filter) const
		{
			for (size_t ind = 0; ind < samplers.size(); ++ind)
				if (samplers[ind].filter == filter)
					return { ind };
			return { (size_t)-1 };
		}
		const texture_sampler& get_sampler(sampler_id id) const
		{
			if (id > samplers.size())
				throw std::runtime_error("Attempting to access an out of bounds sampler");
			return samplers[id].sampler;
		}
	};

	vulkan_sampler_manager* sampler_manager;
	vulkan_image_manager* image_manager;
	struct model
	{
		//instance data
		gpu_mesh target_mesh;
		std::shared_ptr<cpu_mesh<position, normal, uvs>> cpu_local_mesh;
		image_id target_texture;
		sampler_id sampler;

		VkImageView shadow_view;
		sampler_id shadow_sampler;

		glm::mat4 position_matrix;
		glm::mat4 rotation_matrix;
		//unique data
		glm::mat4 model_matrix;
		gpu_data_buffer uniform_buffer;
		VkBuffer camera_buffer;
		VkBuffer shadow_buffer;

		descriptor_set desc_set;
		descriptor_set shadow_descriptor_set;

		bool is_dirty;
		model() {}
		model(descriptor_set_layout descriptor_layout, descriptor_set_layout shadow_descriptor_layout, global_vulkan_instance_state& vulkan_state) : 
			cpu_local_mesh(std::make_unique<cpu_mesh<position, normal, uvs>>()),
			desc_set(vulkan_state),
			shadow_descriptor_set(vulkan_state),
			is_dirty(true)
		{
			desc_set.layout = descriptor_layout;
			shadow_descriptor_set.layout = shadow_descriptor_layout;
		}


		void draw(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout)
		{
			target_mesh.draw(command_buffer, pipeline_layout, desc_set.set);
		}
		void draw_shadows(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout)
		{
			target_mesh.draw_shadows(command_buffer, pipeline_layout, shadow_descriptor_set.set);
		}
		void update_uniform_buffers()
		{
			if (!is_dirty)
				return;
			is_dirty = false;
			rotation_matrix = {};
			model_matrix = rotation_matrix * position_matrix;
			memcpy(uniform_buffer.allocation.alloc.data, &model_matrix, sizeof(model_matrix));
		}
		void upload_data(global_vulkan_instance_state& vulkan_state)
		{
			if (!cpu_local_mesh)
				throw std::runtime_error("cpu_local_mesh is a nullptr.");
			target_mesh = cpu_local_mesh->upload(vulkan_state);

			uniform_buffer = gpu_data_buffer::create_host_visible_buffer(sizeof(model_matrix), vulkan_state);

			create_descriptor_set(vulkan_state);
		}
		void create_descriptor_set(global_vulkan_instance_state& vulkan_state)
		{
			//finalize the descriptor set
			//note: descriptor set binding layouts are guaranteed sorted
			std::vector<VkBuffer> buffers{ uniform_buffer.buffer, camera_buffer, shadow_buffer };
			std::vector<VkBuffer> shadow_buffers{ uniform_buffer.buffer, shadow_buffer };
			std::vector<std::pair<VkSampler, VkImageView>> image_bindings{ { sampler_manager->get_sampler(sampler).sampler, image_manager->get_image(target_texture).image_view },
																			{ sampler_manager->get_sampler(shadow_sampler).sampler, shadow_view}};
			shadow_descriptor_set.create_descriptor_set(shadow_buffers, std::vector<std::pair<VkSampler, VkImageView>>{}, vulkan_state);
			desc_set.create_descriptor_set(buffers, image_bindings, vulkan_state);
		}
		void set_cameras(const camera& cam, const camera& shadow_camera)
		{
			camera_buffer = cam.camera_buffer.buffer;
			shadow_buffer = shadow_camera.camera_buffer.buffer;
		}
		void make_default(global_vulkan_instance_state& vulkan_state)
		{
			/*target_mesh.vertices = {
				//		verts				normals					uvs
				{ { -0.5f, -0.5f, 0.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f } },
				{ { 0.5f, -0.5f, 0.0f },{ 0.0f, 1.0f, 0.0f },{ 0.0f, 1.0f } },
				{ { 0.5f, 0.5f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 1.0f, 1.0f } },
				{ { -0.5f, 0.5f, 0.0f },{ 1.0f, 1.0f, 1.0f },{ 1.0f, 0.0f } },

				{ { -0.5f, -0.5f, -0.5f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 0.0f } },
				{ { 0.5f, -0.5f, -0.5f },{ 0.0f, 1.0f, 0.0f },{ 0.0f, 1.0f } },
				{ { 0.5f, 0.5f, -0.5f },{ 0.0f, 0.0f, 1.0f },{ 1.0f, 1.0f } },
				{ { -0.5f, 0.5f, -0.5f },{ 1.0f, 1.0f, 1.0f },{ 1.0f, 0.0f } }
			};*/
			cpu_local_mesh->get<position>() = 
			{
				{ -0.5f, -0.5f, 0.0f },
				{ 0.5f, -0.5f, 0.0f },
				{ 0.5f, 0.5f, 0.0f },
				{ -0.5f, 0.5f, 0.0f },

				{ -0.5f, -0.5f, -0.5f },
				{ 0.5f, -0.5f, -0.5f },
				{ 0.5f, 0.5f, -0.5f },
				{ -0.5f, 0.5f, -0.5f }
			};
			cpu_local_mesh->get<normal>() =
			{
				{ -1.0f, 1.0f, 0.0f },
				{ 1.0f, 1.0f, 0.0f },
				{ 1.0f, -1.0f, 1.0f },
				{ -1.0f, -1.0f, 1.0f },

				{ 1.0f, 0.0f, 0.0f },
				{ 0.0f, 1.0f, 0.0f },
				{ 0.0f, 0.0f, 1.0f },
				{ 1.0f, 1.0f, 1.0f }
			};
			cpu_local_mesh->get<uvs>() =
			{
				{ 0.0f, 0.0f },
				{ 0.0f, 1.0f },
				{ 1.0f, 1.0f },
				{ 1.0f, 0.0f },
				
				{ 0.0f, 0.0f },
				{ 0.0f, 1.0f },
				{ 1.0f, 1.0f },
				{ 1.0f, 0.0f }
			};

			cpu_local_mesh->indices = { 0, 1, 2, 2, 3, 0,
				4, 5, 6, 6, 7, 4 };
			position_matrix = glm::translate(glm::mat4(), glm::vec3());
			rotation_matrix = {};
			model_matrix = position_matrix*rotation_matrix;



			target_texture = image_manager->create_image("ayy lmao.jpg", image_data::RGBA, false, vulkan_state);
			sampler = sampler_manager->create_sampler(texture_sampler::linear, vulkan_state);
			shadow_sampler = sampler_manager->create_sampler(texture_sampler::linear, vulkan_state);
		}
	};
}