#pragma once
#include <vulkan\vulkan.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <set>
#include <vector>
#include <array>
#include <string>
#include <unordered_map>
#ifdef _WIN32
#define STDCALL__ __stdcall
#else
#define STDCALL__
#endif

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\hash.hpp>

namespace spawnx_vulkan
{
	constexpr int NUM_FRAMES_BUFFERED = 2;
}

#include "vulkan_deleter.h"
#include "vulkan_common.h"
#include "misc_helper_functions.h"

#include "vulkan_common_structs.h"

#include "vulkan_memory_manager.h"
#include "image_data.h"
#include "vulkan_image.h"
#include "vertex.h"

#include "vulkan_templated_forward_declarations.h"

#include "global_vulkan_instance_state.h"

#include "gpu_data_buffer.h"


#include "mesh.h"
#include "shader_module.h"
#include "texture_sampler.h"

#include "descriptor_binding_data_buffer.h"
#include "descriptor_binding_image_buffer.h"
#include "descriptor_layout.h"

#include "renderpass.h"
#include "pipeline.h"
#include "framebuffer.h"

#include "camera.h"
#include "model.h"
#include "engine.h"
