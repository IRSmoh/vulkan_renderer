#pragma once
#include "vulkan_renderer.h"
struct framebuffer
{
	vulkan_deleter<VkFramebuffer> buffer;
	framebuffer() {}
	framebuffer(const global_vulkan_instance_state& vulkan_state) : buffer({ vulkan_state.logical_device, vkDestroyFramebuffer }) 
	{}
	static framebuffer create_framebuffer(const vulkan_image& framebuffer_image, uint32_t width, uint32_t height, const renderpass& render_pass, const global_vulkan_instance_state& vulkan_state)
	{
		framebuffer fb(vulkan_state);
		VkFramebufferCreateInfo fb_info{};
		fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fb_info.renderPass = render_pass.render_pass;
		fb_info.attachmentCount = 1;
		fb_info.pAttachments = &framebuffer_image.image_view;
		fb_info.width = width;
		fb_info.height = height;
		fb_info.layers = 1;

		CHECK_VK_RESULT(vkCreateFramebuffer(vulkan_state.logical_device, &fb_info, nullptr, fb.buffer.replace()));

		return fb;
	}
};