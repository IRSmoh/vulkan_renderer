#pragma once
#include "vulkan_renderer.h"


struct vulkan_global_shader_details
{
	VkSampleCountFlagBits sample_count = VK_SAMPLE_COUNT_1_BIT;
	VkBool32 enable_anisotropy = VK_FALSE;
	float max_anisotropy = 16;
};

struct present_graphics_queues
{
	VkQueue graphics, present;
};
struct compute_details
{
	VkQueue queue;
	uint32_t queue_index;
};
struct screen_details
{
	VkRect2D scissor;
	VkViewport viewport;
};

struct swapchain_details
{
	VkExtent2D extent;
	VkFormat image_format;
	vulkan_deleter<VkSwapchainKHR> swapchain;// { logical_device, vkDestroySwapchainKHR };
	std::vector<VkImage> images;
	std::vector<vulkan_deleter<VkImageView>> image_views;
	std::vector<vulkan_deleter<VkFramebuffer>> framebuffers;
	swapchain_details(const vulkan_deleter<VkDevice>& logical_device) : swapchain(logical_device, vkDestroySwapchainKHR) {}
};
struct global_vulkan_instance_state
{
public:
#if NDEBUG
	static constexpr bool enable_validation_layers = false;
#else
	static constexpr bool enable_validation_layers = true;
#endif
	static constexpr const char* app_name = "spawnx_render";

	const std::vector<const char*> validation_layers = { "VK_LAYER_LUNARG_core_validation" };
	const std::vector<const char*> device_extensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

	GLFWwindow* window;

	std::uint32_t window_width = 960;
	std::uint32_t window_height = 720;

	VkPhysicalDevice physical_device = VK_NULL_HANDLE;
	gpu_info gpu_stats;
	present_graphics_queues queues;
	queue_family_indices queue_indices;

	compute_details compute;

	vulkan_deleter<VkInstance> instance{ vkDestroyInstance };

	vulkan_deleter<VkDebugReportCallbackEXT> callback{ instance, destroy_debug_callback_ext };
	vulkan_deleter<VkDevice> logical_device{ vkDestroyDevice };
	vulkan_deleter<VkSurfaceKHR> surface{ instance,vkDestroySurfaceKHR };

	vulkan_deleter<VkCommandPool> command_pool{ logical_device, vkDestroyCommandPool };

	screen_details screen_stats;

	swapchain_details swapchain{logical_device};

	vulkan_global_shader_details shader_details;

	vulkan_image depth_image;


	vulkan_deleter<VkSemaphore> image_available_semaphore{ logical_device, vkDestroySemaphore };
	vulkan_deleter<VkSemaphore> render_finished_semaphore{ logical_device, vkDestroySemaphore };
	vulkan_deleter<VkSemaphore> shadow_finished_semaphore{ logical_device, vkDestroySemaphore };

	std::vector<VkCommandBuffer> command_buffers;


	using engine_recast_ptr = void(*)(void*);
	engine_recast_ptr engine_casting_fnc;
	void* engine_ptr;

	void vulkan_startup(bool start_in_fullscreen)
	{
		init_window(start_in_fullscreen);
		create_instance();
		setup_debug_callback();
		create_surface();
		get_physical_device();
		get_gpu_stats();
		create_logical_device();
	}
private:
	void init_window(bool start_in_fullscreen)
	{
		glfwInit();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		if (start_in_fullscreen)
		{
			auto monitor = glfwGetPrimaryMonitor();
			auto mode = glfwGetVideoMode(monitor);

			glfwWindowHint(GLFW_RED_BITS, mode->redBits);
			glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
			glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
			glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

			window_height = mode->height;
			window_width = mode->width;

			window = glfwCreateWindow(window_width, window_height, app_name, monitor, nullptr);
		}
		else
			window = glfwCreateWindow(window_width, window_height, app_name, nullptr, nullptr);

		glfwSetWindowUserPointer(window, this);
		glfwSetWindowSizeCallback(window, global_vulkan_instance_state::on_window_resized);

		auto height = window_height;
		auto width = window_width;
		swapchain.extent = { (uint32_t)width, (uint32_t)height };
		screen_stats.scissor = { 0,0,(uint32_t)width,(uint32_t)height };
		screen_stats.viewport = { 0,0,(float)width, (float)height,0.f,1.f };
	}
	void create_instance()
	{
		if (enable_validation_layers && !check_validation_layer_support())
		{
			throw std::runtime_error("Validation layers requested, but not available");
		}

		VkApplicationInfo app_info{};
		app_info.sType				= VK_STRUCTURE_TYPE_APPLICATION_INFO;
		app_info.pApplicationName	= app_name;
		app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		app_info.pEngineName		= "No Engine";
		app_info.engineVersion		= VK_MAKE_VERSION(1, 0, 0);
		app_info.apiVersion			= VK_API_VERSION_1_0;


		auto extensions = get_required_extensions();

		VkInstanceCreateInfo create_info{};
		create_info.sType					= VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		create_info.pApplicationInfo		= &app_info;
		create_info.enabledExtensionCount	= (uint32_t)extensions.size();
		create_info.ppEnabledExtensionNames = extensions.data();

		if (enable_validation_layers)
		{
			create_info.enabledLayerCount	= (uint32_t)validation_layers.size();
			create_info.ppEnabledLayerNames = validation_layers.data();
		}
		else
		{
			create_info.enabledLayerCount = 0;
		}
		CHECK_VK_RESULT(vkCreateInstance(&create_info, nullptr, instance.replace()));
	}
	void setup_debug_callback()
	{
		if (!enable_validation_layers)
			return;

		VkDebugReportCallbackCreateInfoEXT create_info{};
		create_info.sType			= VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		create_info.flags			= VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
		create_info.pfnCallback		= debug_callback;

		CHECK_VK_RESULT(create_debug_report_callback_EXT(instance, &create_info, nullptr, callback.replace()));
	}
	//surface
	void create_surface()
	{
		CHECK_VK_RESULT(glfwCreateWindowSurface(instance, window, nullptr, surface.replace()));
	}
	//physical device
	void get_physical_device()
	{
		uint32_t device_count = 0;
		vkEnumeratePhysicalDevices(instance, &device_count, nullptr);
		if (device_count == 0)
			throw std::runtime_error("No dedicated GPU found");
		std::vector<VkPhysicalDevice> devices(device_count);
		vkEnumeratePhysicalDevices(instance, &device_count, devices.data());

		for (const auto& device : devices)
		{
			if (is_device_suitable(device))
			{
				physical_device = device;
				break;
			}
		}
		if (physical_device == VK_NULL_HANDLE)
			throw std::runtime_error("Could not find a valid GPU to use.");
	}
	//virtual device
	void create_logical_device()
	{
		queue_family_indices indices = find_queue_family_indices(physical_device);
		queue_indices = indices;
		VkDeviceQueueCreateInfo queue_create_info{};
		queue_create_info.sType				= VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queue_create_info.queueFamilyIndex	= indices.graphics_family;
		queue_create_info.queueCount		= 1;

		float queue_priority = 1.0f;
		queue_create_info.pQueuePriorities = &queue_priority;

		VkPhysicalDeviceFeatures device_features = {};
		VkDeviceCreateInfo device_create_info{};
		device_create_info.sType					= VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		device_create_info.pQueueCreateInfos		= &queue_create_info;
		device_create_info.queueCreateInfoCount		= 1;
		device_create_info.pEnabledFeatures			= &device_features;
		device_create_info.enabledExtensionCount	= (uint32_t)device_extensions.size();
		device_create_info.ppEnabledExtensionNames	= device_extensions.data();

		if (enable_validation_layers)
		{
			device_create_info.enabledLayerCount = (uint32_t)validation_layers.size();
			device_create_info.ppEnabledLayerNames = validation_layers.data();
		}
		else
		{
			device_create_info.enabledLayerCount = 0;
		}

		std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
		//most of the time the queues will be the exact same index, but on occassion they aren't. its literally the only reason we don't just skip past this.
		std::set<int> unique_queue_famlies = { indices.graphics_family , indices.present_family };
		for (auto queue_family : unique_queue_famlies)
		{
			VkDeviceQueueCreateInfo queue_create_info{};
			queue_create_info.sType				= VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queue_create_info.queueFamilyIndex	= queue_family;
			queue_create_info.queueCount		= 1;
			queue_create_info.pQueuePriorities	= &queue_priority;
			queue_create_infos.push_back(queue_create_info);
		}

		device_create_info.pQueueCreateInfos = queue_create_infos.data();
		device_create_info.queueCreateInfoCount = (uint32_t)queue_create_infos.size();
		CHECK_VK_RESULT(vkCreateDevice(physical_device, &device_create_info, nullptr, logical_device.replace()));

		vkGetDeviceQueue(logical_device, indices.graphics_family, 0, &queues.graphics);
		vkGetDeviceQueue(logical_device, indices.present_family, 0, &queues.present);
	}
	public:
	//properties
	//easy creation of buffers/queues
	//easy uploading/data transfers
	void create_swapchain()
	{
		swapchain_support_details swapchain_support = query_swapchain_support(physical_device);
		VkSurfaceFormatKHR surface_format = choose_swap_surface_format(swapchain_support.formats);
		VkPresentModeKHR present_mode = choose_swap_present_mode(swapchain_support.present_modes);
		VkExtent2D extent = choose_swap_extent(swapchain_support.capabilities);

		uint32_t image_count = swapchain_support.capabilities.minImageCount + 1;
		if (swapchain_support.capabilities.maxImageCount > 0 && image_count > swapchain_support.capabilities.maxImageCount)
			image_count = swapchain_support.capabilities.maxImageCount;

		VkSwapchainCreateInfoKHR create_info{};
		create_info.sType				= VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		create_info.surface				= surface;
		create_info.minImageCount		= image_count;
		create_info.imageFormat			= surface_format.format;
		create_info.imageColorSpace		= surface_format.colorSpace;
		create_info.imageExtent			= extent;
		create_info.imageArrayLayers	= 1;
		create_info.imageUsage			= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

		queue_family_indices indices = find_queue_family_indices(physical_device);
		uint32_t queue_family_indices[] = { (uint32_t)indices.graphics_family, (uint32_t)indices.present_family };

		if (indices.graphics_family != indices.present_family)
		{
			create_info.imageSharingMode		= VK_SHARING_MODE_CONCURRENT;
			create_info.queueFamilyIndexCount	= 2;
			create_info.pQueueFamilyIndices		= queue_family_indices;
		}
		else
		{
			create_info.imageSharingMode		= VK_SHARING_MODE_EXCLUSIVE;
		}

		create_info.preTransform	= swapchain_support.capabilities.currentTransform;
		create_info.compositeAlpha	= VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		create_info.presentMode		= present_mode;
		create_info.clipped			= VK_TRUE;

		CHECK_VK_RESULT(vkCreateSwapchainKHR(logical_device, &create_info, nullptr, swapchain.swapchain.replace()));

		vkGetSwapchainImagesKHR(logical_device, swapchain.swapchain, &image_count, nullptr);
		swapchain.images.resize(image_count);
		vkGetSwapchainImagesKHR(logical_device, swapchain.swapchain, &image_count, swapchain.images.data());

		swapchain.image_format = surface_format.format;
		swapchain.extent = extent;
	}
	void create_swapchain_image_views()
	{
		swapchain.image_views.resize(swapchain.images.size());
		for (size_t i = 0; i < swapchain.images.size(); ++i)
		{
			swapchain.image_views[i] = { logical_device, vkDestroyImageView };
			create_image_view(swapchain.images[i], swapchain.image_format, VK_IMAGE_ASPECT_COLOR_BIT, swapchain.image_views[i]);
		}
	}
	void create_command_pool()
	{
		queue_family_indices indices = find_queue_family_indices(physical_device);
		VkCommandPoolCreateInfo pool_info{};
		pool_info.sType				= VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		pool_info.queueFamilyIndex	= indices.graphics_family;
		pool_info.flags				= VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

		CHECK_VK_RESULT(vkCreateCommandPool(logical_device, &pool_info, nullptr, command_pool.replace()));
	}
	void create_depth_image()
	{
		depth_image = vulkan_image::create_depth_image(swapchain.extent, *this);
	}
	void create_framebuffers(VkRenderPass renderpass)
	{
		swapchain.framebuffers.resize(swapchain.image_views.size());

		for (size_t i = 0; i < swapchain.image_views.size(); ++i)
		{
			swapchain.framebuffers[i] = { logical_device, vkDestroyFramebuffer };
			std::array<VkImageView, 2> attachments{ swapchain.image_views[i], depth_image.image_view };

			VkFramebufferCreateInfo framebuffer_info{};
			framebuffer_info.sType				= VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebuffer_info.renderPass			= renderpass;
			framebuffer_info.attachmentCount	= (uint32_t)attachments.size();
			framebuffer_info.pAttachments		= attachments.data();
			framebuffer_info.width				= swapchain.extent.width;
			framebuffer_info.height				= swapchain.extent.height;
			framebuffer_info.layers				= 1;

			CHECK_VK_RESULT(vkCreateFramebuffer(logical_device, &framebuffer_info, nullptr, swapchain.framebuffers[i].replace()));
		}
	}
	void create_semaphores()
	{
		VkSemaphoreCreateInfo semaphore_info{};
		semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		CHECK_VK_RESULT(vkCreateSemaphore(logical_device, &semaphore_info, nullptr, image_available_semaphore.replace()));
		CHECK_VK_RESULT(vkCreateSemaphore(logical_device, &semaphore_info, nullptr, render_finished_semaphore.replace()));
		CHECK_VK_RESULT(vkCreateSemaphore(logical_device, &semaphore_info, nullptr, shadow_finished_semaphore.replace()));
	}

	bool is_device_suitable(VkPhysicalDevice device)
	{
		VkPhysicalDeviceProperties device_properties;
		VkPhysicalDeviceLimits device_limits;
		vkGetPhysicalDeviceProperties(device, &device_properties);
		device_limits = device_properties.limits;
		auto max_vert_bindings = device_limits.maxVertexInputBindings;
		return find_queue_family_indices(device).is_complete() && check_device_extension_support(device) && query_swapchain_support(device).is_valid();
	}
	queue_family_indices find_queue_family_indices(VkPhysicalDevice device)
	{
		queue_family_indices indices;
		uint32_t queue_family_count = 0;

		vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, nullptr);
		std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, queue_families.data());

		int queue_index = 0;
		VkBool32 present_support = false;
		for (const auto& queue_family : queue_families)
		{
			vkGetPhysicalDeviceSurfaceSupportKHR(device, queue_index, surface, &present_support);
			if (queue_family.queueCount > 0 && queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT && present_support)
			{
				indices = { queue_index, queue_index };
			}
			++queue_index;
		}

		return indices;
	}
	void find_compute_queue()
	{
		compute.queue_index = -1;
		uint32_t queue_family_count = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_count, nullptr);

		std::vector<VkQueueFamilyProperties> queue_family_props;
		queue_family_props.resize(queue_family_count);
		vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_count, queue_family_props.data());
		
		for (uint32_t i = 0; i < static_cast<uint32_t>(queue_family_props.size()); ++i)
		{
			if ((queue_family_props[i].queueFlags & VK_QUEUE_COMPUTE_BIT) && ((queue_family_props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0))
			{
				compute.queue_index = i;
				break;
			}
		}
		if (compute.queue_index == -1)
		{
			for (uint32_t i = 0; i < static_cast<uint32_t>(queue_family_props.size()); ++i)
			{
				if (queue_family_props[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
				{
					compute.queue_index = i;
					break;
				}
			}
		}

		vkGetDeviceQueue(logical_device, compute.queue_index, 0, &compute.queue);
	}
	bool check_device_extension_support(VkPhysicalDevice device)
	{
		uint32_t extension_count;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, nullptr);
		std::vector<VkExtensionProperties> available_extensions(extension_count);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, available_extensions.data());
		std::set<std::string> required_extensions(device_extensions.begin(), device_extensions.end());

		for (const auto& extension : available_extensions)
		{
			//we want to remove all matches for our required device extensions. if we've managed to remove them all we have all the desired support
			required_extensions.erase(extension.extensionName);
		}
		return required_extensions.empty();
	}
	swapchain_support_details query_swapchain_support(VkPhysicalDevice device)
	{
		swapchain_support_details details;

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);
		uint32_t format_count;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, nullptr);
		if (format_count)
		{
			details.formats.resize(format_count);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, details.formats.data());
		}

		uint32_t present_mode_count;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &present_mode_count, nullptr);
		if (present_mode_count)
		{
			details.present_modes.resize(present_mode_count);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &present_mode_count, details.present_modes.data());
		}
		return details;
	}
	VkSurfaceFormatKHR choose_swap_surface_format(const std::vector<VkSurfaceFormatKHR>& available_formats)
	{
		if (available_formats.size() == 1 && available_formats[0].format == VK_FORMAT_UNDEFINED)
			return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
		for (const auto& available_format : available_formats)
		{
			if (available_format.format == VK_FORMAT_B8G8R8A8_UNORM && available_format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
				return available_format;
		}
		assert(!available_formats.empty());
		return available_formats[0];
	}
	VkPresentModeKHR choose_swap_present_mode(const std::vector<VkPresentModeKHR>& available_present_modes)
	{
		for (const auto& available_present_mode : available_present_modes)
		{
			if (available_present_mode == VK_PRESENT_MODE_MAILBOX_KHR)
			{
				return available_present_mode;
			}
		}
		return VK_PRESENT_MODE_FIFO_KHR;
	}
	VkExtent2D choose_swap_extent(const VkSurfaceCapabilitiesKHR& capabilities)
	{
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
			return capabilities.currentExtent;

		VkExtent2D actual_extent	= { window_width,window_height };
		actual_extent.width			= std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actual_extent.width));
		actual_extent.height		= std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actual_extent.height));

		return actual_extent;
	}
	void create_image_view(VkImage image, VkFormat format, VkImageAspectFlags aspect_flags, vulkan_deleter<VkImageView>& image_view)
	{
		VkImageViewCreateInfo view_info{};
		view_info.sType		= VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		view_info.image		= image;
		view_info.viewType	= VK_IMAGE_VIEW_TYPE_2D;
		view_info.format	= format;

		view_info.subresourceRange.aspectMask		= aspect_flags;
		view_info.subresourceRange.baseMipLevel		= 0;
		view_info.subresourceRange.levelCount		= 1;
		view_info.subresourceRange.baseArrayLayer	= 0;
		view_info.subresourceRange.layerCount		= 1;

		CHECK_VK_RESULT(vkCreateImageView(logical_device, &view_info, nullptr, image_view.replace()));
	}

	void create_image(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, vulkan_deleter<VkImage>& image, vulkan_deleter<VkDeviceMemory>& image_memory)
	{
		VkImageCreateInfo image_info{};
		image_info.sType			= VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		image_info.imageType		= VK_IMAGE_TYPE_2D;
		image_info.extent.width		= width;
		image_info.extent.height	= height;
		image_info.extent.depth		= 1;
		image_info.mipLevels		= 1;
		image_info.arrayLayers		= 1;
		image_info.format			= format;
		image_info.tiling			= tiling;
		image_info.initialLayout	= VK_IMAGE_LAYOUT_UNDEFINED;
		image_info.usage			= usage;
		image_info.samples			= VK_SAMPLE_COUNT_1_BIT;
		image_info.sharingMode		= VK_SHARING_MODE_EXCLUSIVE;

		CHECK_VK_RESULT(vkCreateImage(logical_device, &image_info, nullptr, image.replace()));

		VkMemoryRequirements mem_requirements;
		vkGetImageMemoryRequirements(logical_device, image, &mem_requirements);

		VkMemoryAllocateInfo alloc_info{};
		alloc_info.sType			= VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		alloc_info.allocationSize	= mem_requirements.size;
		alloc_info.memoryTypeIndex	= find_memory_type(mem_requirements.memoryTypeBits, properties);

		CHECK_VK_RESULT(vkAllocateMemory(logical_device, &alloc_info, nullptr, image_memory.replace()));

		vkBindImageMemory(logical_device, image, image_memory, 0);
	}

	static VkResult create_debug_report_callback_EXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* create_info, const VkAllocationCallbacks* allocator, VkDebugReportCallbackEXT* callback)
	{
		auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
		if (func)
		{
			return func(instance, create_info, allocator, callback);
		}
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
	static void STDCALL__ destroy_debug_callback_ext(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* allocator)
	{
		auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
		if (func)
		{
			func(instance, callback, allocator);
		}
	}
	static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT obj_type,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char* layer_prefix,
		const char* msg,
		void* userData)
	{
		std::cerr << "Valiation layer: " << msg << "\n";
		return VK_FALSE;
	}
	static void on_window_resized(GLFWwindow* window, int width, int height)
	{
		if (width == 0 || height == 0)
			return;
		auto app = reinterpret_cast<global_vulkan_instance_state*>(glfwGetWindowUserPointer(window));
		app->window_height			= height;
		app->window_width			= width;
		app->swapchain.extent		= { (uint32_t)width, (uint32_t)height };
		app->screen_stats.scissor	= { 0,0,(uint32_t)width,(uint32_t)height };
		app->screen_stats.viewport	= { 0,0,(float)width, (float)height,0.f,1.f };
		app->engine_casting_fnc(app->engine_ptr);
	}
	bool check_validation_layer_support()
	{
		uint32_t layer_count;
		vkEnumerateInstanceLayerProperties(&layer_count, nullptr);
		//get the currently supported layers
		std::vector<VkLayerProperties> available_layers(layer_count);
		vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

		//scan through all of them to make sure they match our expected debug/validation layer
		for (auto layer_name : validation_layers)
		{
			for (const auto& layer_properties : available_layers)
			{
				if (strcmp(layer_name, layer_properties.layerName) == 0)
					return true;
			}
		}
		return false;
	}
	std::vector<const char*> get_required_extensions()
	{
		std::vector<const char*> extensions;
		unsigned int glfw_extension_count = 0;
		const char** glfw_extensions;
		glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

		for (unsigned int i = 0; i < glfw_extension_count; ++i)
		{
			extensions.push_back(glfw_extensions[i]);
		}
		if (enable_validation_layers)
		{
			extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		}
		return extensions;
	}
	void get_gpu_stats()
	{
		vkGetPhysicalDeviceMemoryProperties(physical_device, &gpu_stats.memory_properties);
		vkGetPhysicalDeviceProperties(physical_device, &gpu_stats.properties);
		vkGetPhysicalDeviceFeatures(physical_device, &gpu_stats.features);
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &gpu_stats.surface_capabilities);
	}
	uint32_t find_memory_type(uint32_t type_filter, VkMemoryPropertyFlags properties) const
	{
		//find our required bit fields.
		for (uint32_t i = 0; i < gpu_stats.memory_properties.memoryTypeCount; ++i)
		{
			if ((type_filter & (1 << i)) && (gpu_stats.memory_properties.memoryTypes[i].propertyFlags & properties) == properties)
				return i;
		}
		throw std::runtime_error("Could not find a valid memory type");
		return 0;
	}
	VkFormat find_supported_format(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) const
	{
		VkFormatProperties properties;
		for (VkFormat format : candidates)
		{
			vkGetPhysicalDeviceFormatProperties(physical_device, format, &properties);
			if (tiling == VK_IMAGE_TILING_LINEAR && (properties.linearTilingFeatures & features) == features)
				return format;
			if (tiling == VK_IMAGE_TILING_OPTIMAL && (properties.optimalTilingFeatures & features) == features)
				return format;
		}

		throw std::runtime_error("Could not find a supported image tiling format");
		return VkFormat::VK_FORMAT_UNDEFINED;
	}
	VkFormat find_depth_format() const
	{
		return find_supported_format({ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
			VK_IMAGE_TILING_OPTIMAL,
			VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
	}
	VkCommandBuffer begin_single_time_commands() const
	{
		VkCommandBufferAllocateInfo alloc_info{};
		alloc_info.sType				= VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		alloc_info.level				= VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		alloc_info.commandPool			= command_pool;
		alloc_info.commandBufferCount	= 1;

		VkCommandBuffer command_buffer;
		vkAllocateCommandBuffers(logical_device, &alloc_info, &command_buffer);

		VkCommandBufferBeginInfo begin_info{};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vkBeginCommandBuffer(command_buffer, &begin_info);

		return command_buffer;
	}
	void end_single_time_commands(VkCommandBuffer command_buffer) const
	{
		vkEndCommandBuffer(command_buffer);

		VkSubmitInfo submit_info{};
		submit_info.sType				= VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submit_info.commandBufferCount	= 1;
		submit_info.pCommandBuffers		= &command_buffer;

		vkQueueSubmit(queues.graphics, 1, &submit_info, VK_NULL_HANDLE);
		vkQueueWaitIdle(queues.graphics);	//should be using a fence and waiting on it instead but oh well.

		vkFreeCommandBuffers(logical_device, command_pool, 1, &command_buffer);
	}
	bool has_stencil_component(VkFormat format) const
	{
		return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
	}
	void transition_image_layout(VkImage image, VkFormat format, VkImageLayout old_layout, VkImageLayout new_layout) const
	{
		VkCommandBuffer command_buffer = begin_single_time_commands();

		VkImageMemoryBarrier barrier{};
		barrier.sType				= VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout			= old_layout;
		barrier.newLayout			= new_layout;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.image				= image;

		if (new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
		{
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
			if (has_stencil_component(format))
				barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
		}
		else
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

		barrier.subresourceRange.baseMipLevel	= 0;
		barrier.subresourceRange.levelCount		= 1;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount		= 1;

		VkPipelineStageFlags source_stage;
		VkPipelineStageFlags destination_stage;

		if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED && new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		{
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		}
		else if (old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
		{
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		}
		else if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED && new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
		{
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

			source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destination_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		}
		else
		{
			throw std::invalid_argument("unsupported layout transition");
		}

		vkCmdPipelineBarrier(command_buffer, 
			source_stage, destination_stage,
			0,
			0, nullptr,
			0, nullptr,
			1, &barrier);

		end_single_time_commands(command_buffer);
	}
	void copy_buffer_to_image(VkBuffer buffer, VkImage image, VkExtent3D image_dims, VkImageSubresourceLayers image_subresource) const
	{
		VkCommandBuffer command_buffer = begin_single_time_commands();

		VkBufferImageCopy region{};
		region.bufferOffset			= 0;
		region.bufferImageHeight	= 0;	// ??
		region.bufferImageHeight	= 0;	// ??
		region.imageSubresource		= image_subresource;
		region.imageOffset			= { 0,0,0 };
		region.imageExtent			= image_dims;

		vkCmdCopyBufferToImage(command_buffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
		end_single_time_commands(command_buffer);
	}
	void copy_buffer(VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size, VkCommandBuffer command_buffer) const
	{
		//need to find a away to preserve this struct after exiting in a sane way.
		//if everything is used this way... vkCmdCopyBuffer will asyncly try to access our struct... which is now garbaged.
		VkBufferCopy copy_region{};
		copy_region.size = size;
		vkCmdCopyBuffer(command_buffer, src_buffer, dst_buffer, 1, &copy_region);
	}
	void copy_buffer_immediate(VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size) const
	{
		VkCommandBuffer command_buffer = begin_single_time_commands();
		VkBufferCopy copy_region{};
		copy_region.size = size;
		vkCmdCopyBuffer(command_buffer, src_buffer, dst_buffer, 1, &copy_region);
		end_single_time_commands(command_buffer);
	}
};