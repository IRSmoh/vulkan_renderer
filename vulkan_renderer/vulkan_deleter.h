#pragma once
#include <vulkan/vulkan.h>
#include <functional>
#include <memory>
#include <type_traits>
template<typename type>
struct vulkan_deleter
{
	vulkan_deleter() : vulkan_deleter([](type, VkAllocationCallbacks*) {}) {}
	vulkan_deleter(std::function<void(type, VkAllocationCallbacks*)> deletef)
		: resources(std::make_shared<resource_block>(resource_block{}))
	{
		resources->deleter = [=](type obj) {deletef(obj, nullptr); };
	}
	vulkan_deleter(const vulkan_deleter<VkInstance>& instance, std::function<void(VkInstance, type, VkAllocationCallbacks*)> deletef)
		: resources(std::make_shared<resource_block>(resource_block{}))
	{
		resources->deleter = [&instance, deletef](type obj) {deletef(instance, obj, nullptr); };
	}
	vulkan_deleter(const vulkan_deleter<VkDevice>& device, std::function<void(VkDevice, type, VkAllocationCallbacks*)> deletef)
		: resources(std::make_shared<resource_block>(resource_block{}))
	{
		resources->deleter = [&device, deletef](type obj) { deletef(device, obj, nullptr); };
	}

	const type* operator&() const
	{
		return &resources->object;
	}
	type* replace()
	{
		resources->cleanup();
		return &resources->object;
	}
	operator const type&() const
	{
		return resources->object;
	}

	template<typename V>
	bool operator==(V right)
	{
		return resources->object == type(right);
	}
private:
	//I feel like I have to explain myself here; because this is a stupid work around.
	//I want; no. I need whatever vk_object_type that we are passed to sanely be used with a vector
	//if the vector or any datatype resizes and causes a copy to occur _anywhere_ we'll have our resource deleted when we're not expecting it to.
	//typically (and if things worked how the should) we'd use a unique_ptr to force it to be move only. and things would be well in the world.
	//unfortunately VS2017 does not have a functional impelemnation of a move only vector...
	//doing a simple minimal test outside of any 'crazy' ness causes an internal compiler error on trying to resize a move only type.
	//... we'll be resizing a lot; and internal resizing would happen either way.
	//
	//So instead we're stuck with protecting our resource through the shittiest way and using a shared_ptr to make sure its not deleted too early.

	struct resource_block
	{
		type object{};
		std::function<void(type)> deleter;
		~resource_block()
		{
			cleanup();
		}
		void cleanup()
		{
			if (object)
			{
				deleter(object);
			}
			object = {};
		}
	};

	std::shared_ptr<resource_block> resources;
};