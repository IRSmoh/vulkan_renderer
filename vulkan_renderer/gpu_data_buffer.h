#pragma once
#include <vulkan\vulkan.h>

#include "vulkan_deleter.h"
#include "vulkan_common.h"

#include "global_vulkan_instance_state.h"
struct gpu_data_buffer
{
	vulkan_deleter<VkBuffer> buffer;
	vulkan_allocation_wrapper allocation;


	gpu_data_buffer() {}
	gpu_data_buffer(const global_vulkan_instance_state& vulkan_state)
		: buffer( vulkan_state.logical_device, vkDestroyBuffer )
	{}

	static gpu_data_buffer create_buffer(VkDeviceSize buffer_size, VkBufferUsageFlags usage, vulkan_memory_usage_e memory_usage, const global_vulkan_instance_state& vulkan_state)
	{
		gpu_data_buffer data_buffer(vulkan_state);
		VkBufferCreateInfo buffer_info{};
		buffer_info.sType		= VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		buffer_info.size		= buffer_size;
		buffer_info.usage		= usage;
		buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		CHECK_VK_RESULT(vkCreateBuffer(vulkan_state.logical_device, &buffer_info, nullptr, data_buffer.buffer.replace()));

		VkMemoryRequirements mem_reqs;
		vkGetBufferMemoryRequirements(vulkan_state.logical_device, data_buffer.buffer, &mem_reqs);

		data_buffer.allocation = vk_allocator->allocate(mem_reqs.size, mem_reqs.alignment, mem_reqs.memoryTypeBits, memory_usage, VULKAN_ALLOCATION_TYPE_BUFFER);

		vkBindBufferMemory(vulkan_state.logical_device, data_buffer.buffer, data_buffer.allocation.alloc.device_memory, data_buffer.allocation.alloc.offset);
		return data_buffer;
	}
	static gpu_data_buffer create_staging_buffer(VkDeviceSize buffer_size, const global_vulkan_instance_state& vulkan_state)
	{
		return create_buffer(buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VULKAN_MEMORY_USAGE_CPU_ONLY, vulkan_state);
	}
	static gpu_data_buffer create_gpu_dst_buffer(VkDeviceSize buffer_size, const global_vulkan_instance_state& vulkan_state)
	{
		return create_buffer(buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VULKAN_MEMORY_USAGE_GPU_ONLY, vulkan_state);
	}
	static gpu_data_buffer create_host_visible_buffer(VkDeviceSize buffer_size, const global_vulkan_instance_state& vulkan_state)
	{
		return create_buffer(buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VULKAN_MEMORY_USAGE_CPU_TO_GPU, vulkan_state);
	}
	static gpu_data_buffer create_gpu_only_buffer(VkDeviceSize buffer_size, VkBufferUsageFlags buffer_usage, const global_vulkan_instance_state& vulkan_state)
	{
		return create_buffer(buffer_size, buffer_usage, VULKAN_MEMORY_USAGE_GPU_ONLY, vulkan_state);
	}
	//callable is of the form...
	//auto lam = [](copy_dst, copy_src){...};
	//lambda can have whatever state needed.
	//this will be called with the staging buffer as the destination, some source, and will run before uploading the data to the gpu
	//basically its useful if you need to upload disjoint arrays/non contigous data that is mostly* in contiguous blocks.
	//e.g. arrays of arrays type**
	//buffer_data is directly fed to the calling lambda, if you intend to ignore it (aka the copy src) you can safely pass in a nullptr.
	template<typename buffer_type, typename callable_type>
	static gpu_data_buffer upload_data(buffer_type buffer_data, std::size_t buffer_byte_size, const callable_type& callable, VkBufferUsageFlags buffer_flags, const global_vulkan_instance_state& vulkan_state)
	{
		gpu_data_buffer data_buffer(vulkan_state);
		gpu_data_buffer staging_buffer(vulkan_state);
		VkDeviceSize buffer_size = buffer_byte_size;

		staging_buffer = create_buffer(buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VULKAN_MEMORY_USAGE_CPU_ONLY, vulkan_state);

		callable(staging_buffer.allocation.alloc.data, buffer_data);

		data_buffer = create_buffer(buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | buffer_flags, VULKAN_MEMORY_USAGE_GPU_ONLY, vulkan_state);
		vulkan_state.copy_buffer_immediate(staging_buffer.buffer, data_buffer.buffer, buffer_size);

		return data_buffer;
	}
	template<typename type>
	static gpu_data_buffer upload_data(const type* buffer_data, std::size_t buffer_elem_count, VkBufferUsageFlags buffer_flags, const global_vulkan_instance_state& vulkan_state)
	{
		std::size_t buffer_size = buffer_elem_count * sizeof(type);
		auto copy_lam = [buffer_size](auto buffer_dst, auto buffer_src)
		{
			//memcpy(staging_buffer.allocation.alloc.data, buffer_data, (size_t)buffer_size);
			memcpy(buffer_dst, buffer_src, (size_t)buffer_size);
		};
		return upload_data(buffer_data, buffer_size, copy_lam, buffer_flags, vulkan_state);
	}

	template<typename type>
	static gpu_data_buffer upload_data(const std::vector<type>& buffer_data, VkBufferUsageFlags buffer_flags, const global_vulkan_instance_state& vulkan_state)
	{
		return upload_data(buffer_data.data(), buffer_data.size(), buffer_flags, vulkan_state);
	}

	template<typename type>
	static gpu_data_buffer upload_data_immediate(const std::vector<type>& buffer_data, VkBufferUsageFlags buffer_flags, global_vulkan_instance_state& vulkan_state)
	{
		VkCommandBuffer command_buffer = vulkan_state.begin_single_time_commands();
		gpu_data_buffer gpu_buffer = upload_data(buffer_data, buffer_flags, command_buffer, vulkan_state);
		vulkan_state.end_single_time_commands(command_buffer);
		return gpu_buffer;
	}
};