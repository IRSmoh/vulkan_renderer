#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
namespace spawnx_vulkan
{
	std::vector<char> read_from_file(const std::string& filename)
	{
		std::ifstream file(filename, std::ios::ate | std::ios::binary);
		if (!file.is_open())
		{
			throw std::runtime_error("Could not open file: " + filename);
		}
		size_t file_size = (size_t)file.tellg();
		std::vector<char> buffer(file_size);
		file.seekg(0);
		file.read(buffer.data(), file_size);
		file.close();
		return buffer;
	}
	template<typename functor_type, typename tuple_type>
	void apply(tuple_type data_to_fold, functor_type&& functor)
	{
		apply_impl(std::forward<tuple_type>(data_to_fold), functor, std::make_index_sequence<std::tuple_size<tuple_type>::value>{});
	}
	template<typename functor_type, typename tuple_type, size_t... inds>
	void apply_impl(tuple_type&& data_to_fold, functor_type&& functor, std::index_sequence<inds...>)
	{
		(functor(std::get<inds>(data_to_fold)), ...);
	}
}