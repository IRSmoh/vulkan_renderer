#pragma once
#include <vulkan\vulkan.h>
#include <vector>
#include <list>
#include <array>
#include <memory>
#include "vulkan_renderer.h"
struct command_buffer_data_holder
{
private:
	struct block
	{
		size_t size;
		size_t offset;
		std::unique_ptr<void, decltype(&free)> data_block;
		block(size_t max_size) :
			size(max_size), offset(0), data_block(malloc(size), &free)
		{}
		template<typename type, typename assignable_type = typename std::remove_cv<type>::type>
		type* try_add(type&& data)
		{
			if (offset + sizeof(assignable_type) > size)
				return nullptr;
			if (!data_block.get())
				return nullptr;

			*(assignable_type*)((char*)data_block.get() + offset) = std::forward<type>(data);
			offset += sizeof(type);
			return (type*)((char*)data_block.get() + offset - sizeof(type));
		}
	};
	std::list<block>					stored_pod_data;		//the data must not be moved around ever or dangling pointers will occur.
	std::vector<std::shared_ptr<void>>	stored_complex_data;	//since the data is a shared_ptr things won't be deleted when being moved.
public:
	VkCommandBuffer target_buffer;

	constexpr static size_t block_size = 128;

	command_buffer_data_holder(VkCommandBuffer target)
		: target_buffer(target)
	{
		stored_pod_data.emplace_back(block(block_size));
	}
	operator VkCommandBuffer()
	{
		return target_buffer;
	}
	template<typename type, typename actual_type = typename std::remove_reference<type>::type, std::enable_if_t<std::is_pod<actual_type>::value, bool> = true>
	actual_type* add(type&& data)
	{
		if (sizeof(actual_type) > block_size)
		{
			stored_complex_data.push_back(std::make_shared<typename std::remove_const<actual_type>::type>(data));
			return (actual_type*)stored_complex_data.back().get();
		}
		auto tmp_ptr = stored_pod_data.back().template try_add<actual_type>(std::forward<actual_type>(data));
		if (!tmp_ptr)
		{
			stored_pod_data.emplace_back(block(block_size));
			return stored_pod_data.back().template try_add<actual_type>(std::forward<actual_type>(data));
		}
		return tmp_ptr;
	}
	template<typename type, typename actual_type = typename std::remove_reference<type>::type, std::enable_if_t<!std::is_pod<actual_type>::value, bool> = false>
	actual_type* add(type&& data)
	{
		stored_complex_data.push_back(std::make_shared<typename std::remove_const<actual_type>::type>(data));
		return (actual_type*)stored_complex_data.back().get();
	}
	template<typename type, size_t N>
	decltype(auto) add(type(&data)[N])
	{
		stored_complex_data.push_back(std::make_shared<std::array<type, N>>(to_array(data)));
		return (type*)stored_complex_data.back().get();
	}
private:
	template<typename type, std::size_t N, std::size_t... inds>
	constexpr static std::array<type, N> to_array(type(&data)[N], std::index_sequence<inds...>)
	{
		return { data[inds]... };
	}
	template<typename type, std::size_t N>
	constexpr static std::array<type, N> to_array(type(&data)[N])
	{
		return to_array(data, std::make_index_sequence<N>{});
	}

};