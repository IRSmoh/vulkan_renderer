#pragma once
#include "vulkan_renderer.h"

namespace spawnx_vulkan
{
	struct staging_buffer
	{
		staging_buffer() :
			submitted(false),
			command_buffer(VK_NULL_HANDLE),
			buffer(VK_NULL_HANDLE),
			fence(VK_NULL_HANDLE),
			offset(0),
			data(nullptr)
		{}

		bool							submitted;
		vulkan_deleter<VkCommandBuffer> command_buffer;
		vulkan_deleter<VkBuffer>		buffer;
		vulkan_deleter<VkFence>			fence;
		VkDeviceSize					offset;
		void*							data;
	};

	class vulkan_staging_manager
	{
		constexpr static size_t			DEFAULT_BUFFER_SIZE = 64 * 1024 * 1024;

		size_t							max_buffer_size;
		int								current_buffer_ind;
		void*							mapped_data;
		vulkan_deleter<VkDeviceMemory>	device_memory;
		VkCommandPool					command_pool;

		VkDevice						logical_device;
		VkQueue							graphics_queue;
		std::array<staging_buffer, NUM_FRAMES_BUFFERED> buffers;

	public:
		vulkan_staging_manager() {}
		~vulkan_staging_manager() {}

		void init(const global_vulkan_instance_state& vulkan_state)
		{
			logical_device		= vulkan_state.logical_device;
			graphics_queue		= vulkan_state.queues.graphics;
			max_buffer_size		= DEFAULT_BUFFER_SIZE;


			VkBufferCreateInfo buffer_create_info{};
			buffer_create_info.sType	= VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
			buffer_create_info.size		= max_buffer_size;
			buffer_create_info.usage	= VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

			for (auto& curr_staging_buffer : buffers)
			{
				curr_staging_buffer.offset = 0;
				curr_staging_buffer.buffer = { vulkan_state.logical_device,vkDestroyBuffer };
				CHECK_VK_RESULT(vkCreateBuffer(vulkan_state.logical_device, &buffer_create_info, nullptr, curr_staging_buffer.buffer.replace()));
			}

			VkMemoryRequirements memory_reqs;
			vkGetBufferMemoryRequirements(vulkan_state.logical_device, buffers[0].buffer, &memory_reqs);

			const VkDeviceSize align_mod		= memory_reqs.size % memory_reqs.alignment;
			const VkDeviceSize aligned_size		= (align_mod == 0) ? memory_reqs.size : (memory_reqs.size + memory_reqs.alignment - align_mod);

			VkMemoryAllocateInfo memory_alloc_info{};
			memory_alloc_info.sType				= VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			memory_alloc_info.allocationSize	= aligned_size * NUM_FRAMES_BUFFERED;
			memory_alloc_info.memoryTypeIndex	= find_memory_type_index(memory_reqs.memoryTypeBits, VULKAN_MEMORY_USAGE_CPU_TO_GPU, vulkan_state.gpu_stats);

			device_memory = { vulkan_state.logical_device,[](VkDevice logical_device, VkDeviceMemory device_memory, VkAllocationCallbacks*) {vkUnmapMemory(logical_device,device_memory); } };
			CHECK_VK_RESULT(vkAllocateMemory(vulkan_state.logical_device, &memory_alloc_info, nullptr, device_memory.replace()));

			for (int i = 0; i < buffers.size(); ++i)
			{
				CHECK_VK_RESULT(vkBindBufferMemory(vulkan_state.logical_device, buffers[i].buffer, device_memory, i*aligned_size));
			}

			CHECK_VK_RESULT(vkMapMemory(vulkan_state.logical_device, device_memory, 0, aligned_size*NUM_FRAMES_BUFFERED, 0, &mapped_data));

			VkCommandPoolCreateInfo command_pool_create_info{};
			command_pool_create_info.sType				= VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			command_pool_create_info.flags				= VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
			command_pool_create_info.queueFamilyIndex	= vulkan_state.queue_indices.graphics_family;

			CHECK_VK_RESULT(vkCreateCommandPool(vulkan_state.logical_device, &command_pool_create_info, nullptr, &command_pool));
			
			VkCommandBufferAllocateInfo command_buffer_alloc_info{};
			command_buffer_alloc_info.sType					= VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			command_buffer_alloc_info.commandPool			= command_pool;
			command_buffer_alloc_info.commandBufferCount	= 1;

			VkFenceCreateInfo fence_create_info{};
			fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

			VkCommandBufferBeginInfo command_buffer_begin_info{};
			command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

			for (int i = 0; i < NUM_FRAMES_BUFFERED; ++i)
			{
				buffers[i].command_buffer = { [logical_device = vulkan_state.logical_device, command_pool = command_pool](VkCommandBuffer command_buffer,VkAllocationCallbacks*) {
					vkFreeCommandBuffers(logical_device,command_pool,1,&command_buffer);
				} };

				CHECK_VK_RESULT(vkAllocateCommandBuffers(vulkan_state.logical_device, &command_buffer_alloc_info, buffers[i].command_buffer.replace()));

				buffers[i].fence = { vulkan_state.logical_device,vkDestroyFence };
				CHECK_VK_RESULT(vkCreateFence(vulkan_state.logical_device, &fence_create_info, nullptr, buffers[i].fence.replace()));
				CHECK_VK_RESULT(vkBeginCommandBuffer(buffers[i].command_buffer, &command_buffer_begin_info));

				buffers[i].data = (char*)mapped_data + (i*aligned_size);
			}

		}
		void shutdown()
		{
			//unneeded everything cleans itself up.
		}
		void* stage(int size, int alignment, VkCommandBuffer& command_buffer, VkBuffer& buffer, int& buffer_offset)
		{
			if (size > max_buffer_size)
				throw std::runtime_error("Failed to allocate in gpu transfer buffer, as max_buffer_size is too small.");

			staging_buffer& stage = buffers[current_buffer_ind];
			const int align_mod = stage.offset% alignment;
			stage.offset = ((stage.offset % alignment) == 0) ? stage.offset : (stage.offset + alignment - align_mod);

			if (stage.offset + size >= max_buffer_size && !stage.submitted)
				flush();
			stage = buffers[current_buffer_ind];

			if (stage.submitted)
			{
				wait(stage);
			}
			command_buffer	= stage.command_buffer;
			buffer			= stage.buffer;
			buffer_offset	= stage.offset;

			void* data = (char*)(stage.data) + stage.offset;
			stage.offset += size;

			return data;
		}
		void flush()
		{
			staging_buffer& stage = buffers[current_buffer_ind];
			if (stage.submitted || stage.offset == 0)
				return;

			VkMemoryBarrier barrier{};
			barrier.sType			= VK_STRUCTURE_TYPE_MEMORY_BARRIER;
			barrier.srcAccessMask	= VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask	= VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT | VK_ACCESS_INDEX_READ_BIT;

			vkCmdPipelineBarrier(
				stage.command_buffer,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
				0, 1, &barrier, 0, nullptr, 0, nullptr);
			vkEndCommandBuffer(stage.command_buffer);

			VkMappedMemoryRange memory_range{};
			memory_range.sType		= VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
			memory_range.memory		= device_memory;
			memory_range.size		= VK_WHOLE_SIZE;
			vkFlushMappedMemoryRanges(logical_device, 1, &memory_range);

			VkSubmitInfo submit_info{};
			submit_info.sType				= VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submit_info.commandBufferCount	= 1;
			submit_info.pCommandBuffers		= &stage.command_buffer;

			vkQueueSubmit(graphics_queue, 1, &submit_info, stage.fence);
			stage.submitted = true;
			current_buffer_ind = (current_buffer_ind + 1) % NUM_FRAMES_BUFFERED;
		}
	private:
		void wait(staging_buffer& stage)
		{
			if (stage.submitted == false)
				return;
			CHECK_VK_RESULT(vkWaitForFences(logical_device, 1, &stage.fence, VK_TRUE, UINT64_MAX));
			CHECK_VK_RESULT(vkResetFences(logical_device, 1, &stage.fence));

			stage.offset = 0;
			stage.submitted = false;

			VkCommandBufferBeginInfo command_buffer_begin_info{};
			command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

			CHECK_VK_RESULT(vkBeginCommandBuffer(stage.command_buffer, &command_buffer_begin_info));
		}


	};
}
