#pragma once
#include <vulkan\vulkan.h>
#include <memory>

#include "meta_templates.h"
#include "vulkan_deleter.h"
#include "shader_module.h"
#include "descriptor_layout.h"
#include "renderpass.h"
struct pipeline
{
	//shader
	std::vector<shader_module> shader_modules;
	VkRenderPass render_pass;
	vulkan_deleter<VkPipelineLayout> pipeline_layout;
	vulkan_deleter<VkPipeline> graphics_pipeline;
	descriptor_set_layout shader_descriptor_set_layout;

	VkPolygonMode poly_mode;
	
	pipeline() {}
	pipeline(const global_vulkan_instance_state& vulkan_state, VkPolygonMode poly_mode_) :
		shader_descriptor_set_layout(vulkan_state), poly_mode(poly_mode_)
	{
		pipeline_layout = { vulkan_state.logical_device, vkDestroyPipelineLayout };
		graphics_pipeline = { vulkan_state.logical_device, vkDestroyPipeline };
	}


	struct pipeline_create_state
	{
		VkPipelineVertexInputStateCreateInfo	vertex_input_info;
		VkPipelineInputAssemblyStateCreateInfo	input_assembly;
		VkViewport								viewport;
		VkRect2D								scissor;
		VkPipelineViewportStateCreateInfo		viewport_state;
		VkPipelineRasterizationStateCreateInfo	rasterizer;
		VkPipelineMultisampleStateCreateInfo	multisampling;
		VkPipelineDepthStencilStateCreateInfo	depth_stencil_info;
		VkPipelineColorBlendAttachmentState		color_blend_attachment;
		VkPipelineColorBlendStateCreateInfo		color_blending;
		VkPipelineLayoutCreateInfo				pipeline_layout_info;
		std::vector<VkDynamicState>				dynamic_states;
		VkPipelineDynamicStateCreateInfo		dynamic_state;

		bool									use_blending;
		//note to self: delay assigning pointers until this block is actually submitted to the pipeline,
		//or we may for some inane reason have copies/moves of it performed and thus miss matched pointers.
	};


	pipeline_create_state create_default_graphics_pipeline_state(bool use_blending, VkCullModeFlags cull_mode, const global_vulkan_instance_state& vulkan_state)
	{
		pipeline_create_state pipeline_state{};

		pipeline_state.use_blending = use_blending;
		//input assembly
		pipeline_state.input_assembly.sType						= VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		pipeline_state.input_assembly.topology					= VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		pipeline_state.input_assembly.primitiveRestartEnable	= VK_FALSE;

		//view port
		pipeline_state.viewport.x			= 0.0f;
		pipeline_state.viewport.y			= 0.0f;
		pipeline_state.viewport.width		= (float)vulkan_state.window_width;
		pipeline_state.viewport.height		= (float)vulkan_state.window_height;
		pipeline_state.viewport.minDepth	= 0.0f;
		pipeline_state.viewport.maxDepth	= 1.0f;


		//scissor
		pipeline_state.scissor.offset = { 0,0 };
		pipeline_state.scissor.extent = vulkan_state.swapchain.extent;


		//rasterizer
		pipeline_state.rasterizer.sType						= VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		pipeline_state.rasterizer.depthClampEnable			= VK_FALSE;
		pipeline_state.rasterizer.rasterizerDiscardEnable	= VK_FALSE;
		pipeline_state.rasterizer.polygonMode				= poly_mode;

		pipeline_state.rasterizer.lineWidth = 1.0f;
		pipeline_state.rasterizer.cullMode	= cull_mode;
		pipeline_state.rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;


		//multisampling
		pipeline_state.multisampling.sType					= VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		pipeline_state.multisampling.sampleShadingEnable	= VK_FALSE;
		pipeline_state.multisampling.rasterizationSamples	= vulkan_state.shader_details.sample_count;


		//depth stencil..
		pipeline_state.depth_stencil_info.sType					= VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		pipeline_state.depth_stencil_info.depthTestEnable		= VK_TRUE;
		pipeline_state.depth_stencil_info.depthWriteEnable		= VK_TRUE;
		pipeline_state.depth_stencil_info.depthCompareOp		= VK_COMPARE_OP_LESS;
		pipeline_state.depth_stencil_info.depthBoundsTestEnable = VK_FALSE;

		pipeline_state.depth_stencil_info.stencilTestEnable		= VK_FALSE;


		//color blending attachments
		pipeline_state.color_blend_attachment.colorWriteMask	= VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		pipeline_state.color_blend_attachment.blendEnable		= VK_FALSE;


		//color blending..
		pipeline_state.color_blending.sType				= VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		pipeline_state.color_blending.logicOpEnable		= VK_FALSE;
		pipeline_state.color_blending.logicOp			= VK_LOGIC_OP_COPY;
		pipeline_state.color_blending.attachmentCount	= 1;
		//pipeline_state.color_blending.pAttachments = &pipeline_state.color_blend_attachment; //purprosefully delayed
		pipeline_state.color_blending.blendConstants[0] = 0.0f;
		pipeline_state.color_blending.blendConstants[1] = 0.0f;
		pipeline_state.color_blending.blendConstants[2] = 0.0f;
		pipeline_state.color_blending.blendConstants[3] = 0.0f;


		//pipeline layout
		pipeline_state.pipeline_layout_info.sType			= VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipeline_state.pipeline_layout_info.setLayoutCount	= 1;
		pipeline_state.pipeline_layout_info.pSetLayouts		= &shader_descriptor_set_layout.set_layout;	//since this one is a data member of pipeline we can go ahead and assume its not moved/edited.

		pipeline_state.dynamic_states		= { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
		pipeline_state.dynamic_state.sType	= VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		pipeline_state.dynamic_state.pNext	= nullptr;
		pipeline_state.dynamic_state.flags	= 0;
		//pipeline_state.dynamic_state.dynamicStateCount = pipeline_state.dynamic_states.size();
		//pipeline_state.dynamic_state.pDynamicStates = pipeline_state.dynamic_states.data();

		//what do I want to do with this? does it also get delayed?
		//CHECK_VK_RESULT(vkCreatePipelineLayout(vulkan_state.logical_device, &pipeline_layout_info, nullptr, pipeline_layout.replace()));
		return pipeline_state;
	}
	template<typename vertex_binding_description_array_type, typename vertex_attribute_description_array_type,
		std::enable_if_t<spawnx_vulkan::is_array_container<const vertex_binding_description_array_type>::value &&
		spawnx_vulkan::is_array_container<const vertex_attribute_description_array_type>::value, bool> = true>
	void create_graphics_pipeline(
		pipeline_create_state&& pipeline_state,
		const vertex_binding_description_array_type& binding_descriptions,
		const vertex_attribute_description_array_type& attribute_descriptions,
		const global_vulkan_instance_state& state)
	{
		std::vector<VkPipelineShaderStageCreateInfo> shader_stage_infos;
		for (auto& module : shader_modules)
		{
			shader_stage_infos.push_back(module.shader_create_info);
		}

		VkPipelineVertexInputStateCreateInfo vertex_input_info{};	//e.g. what data we're passing to our combined shader program (re: in variables)
		vertex_input_info.sType								= VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertex_input_info.vertexBindingDescriptionCount		= (uint32_t)binding_descriptions.size();
		vertex_input_info.pVertexBindingDescriptions		= binding_descriptions.data();

		vertex_input_info.vertexAttributeDescriptionCount	= (uint32_t)attribute_descriptions.size();
		vertex_input_info.pVertexAttributeDescriptions		= attribute_descriptions.data();

		VkPipelineViewportStateCreateInfo viewport_state{};
		viewport_state.sType			= VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewport_state.viewportCount	= 1;
		viewport_state.pViewports		= &pipeline_state.viewport;
		viewport_state.scissorCount		= 1;
		viewport_state.pScissors		= &pipeline_state.scissor;

		//delayed from before.
		pipeline_state.color_blending.pAttachments		= &pipeline_state.color_blend_attachment;

		pipeline_state.dynamic_state.dynamicStateCount	= (uint32_t)pipeline_state.dynamic_states.size();
		pipeline_state.dynamic_state.pDynamicStates		= pipeline_state.dynamic_states.data();

		CHECK_VK_RESULT(vkCreatePipelineLayout(state.logical_device, &pipeline_state.pipeline_layout_info, nullptr, pipeline_layout.replace()));

		VkGraphicsPipelineCreateInfo pipeline_info{};
		pipeline_info.sType			= VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipeline_info.stageCount	= (uint32_t)shader_stage_infos.size();
		pipeline_info.pStages		= shader_stage_infos.data();

		pipeline_info.pVertexInputState		= &vertex_input_info;
		pipeline_info.pViewportState		= &viewport_state;
		pipeline_info.pInputAssemblyState	= &pipeline_state.input_assembly;
		pipeline_info.pRasterizationState	= &pipeline_state.rasterizer;
		pipeline_info.pMultisampleState		= &pipeline_state.multisampling;
		pipeline_info.pDepthStencilState	= &pipeline_state.depth_stencil_info;
		pipeline_info.pColorBlendState		= pipeline_state.use_blending ? &pipeline_state.color_blending : nullptr;
		pipeline_info.pDynamicState			= &pipeline_state.dynamic_state;

		pipeline_info.layout		= pipeline_layout;
		pipeline_info.renderPass	= render_pass;
		pipeline_info.subpass		= 0;

		pipeline_info.basePipelineHandle		= VK_NULL_HANDLE;
		//pipeline_info.basePipelineIndex		= -1;

		CHECK_VK_RESULT(vkCreateGraphicsPipelines(state.logical_device, VK_NULL_HANDLE, 1, &pipeline_info, nullptr, graphics_pipeline.replace()));
	}
	template<typename vertex_binding_description_array_type, typename vertex_attribute_description_array_type, 
		std::enable_if_t<spawnx_vulkan::is_array_container<const vertex_binding_description_array_type>::value &&
		spawnx_vulkan::is_array_container<const vertex_attribute_description_array_type>::value, bool> = true>
	void create_graphics_pipeline(const vertex_binding_description_array_type& binding_descriptions, const vertex_attribute_description_array_type& attribute_descriptions, bool use_color, VkCullModeFlags cull_mode, const global_vulkan_instance_state& vulkan_state)
	{
		auto pipeline_state = create_default_graphics_pipeline_state(use_color, cull_mode, vulkan_state);
		create_graphics_pipeline(std::move(pipeline_state), binding_descriptions, attribute_descriptions, vulkan_state);
	}
	template<typename vertex_type>
	static pipeline create_pipeline(const std::string& vertex_filename, const std::string& fragment_file,
		descriptor_set_layout descriptor_layout,
		VkRenderPass render_pass,
		VkPolygonMode poly_mode,
		const global_vulkan_instance_state& vulkan_state)
	{
		pipeline pipeline(vulkan_state, poly_mode);

		pipeline.render_pass = render_pass;

		pipeline.shader_modules.push_back(shader_module::load_vertex_shader(vertex_filename, vulkan_state));
		pipeline.shader_modules.push_back(shader_module::load_fragment_shader(fragment_file, vulkan_state));

		pipeline.shader_descriptor_set_layout = descriptor_layout;
		pipeline.create_graphics_pipeline(vertex_type::get_binding_description(), vertex_type::get_attribute_descriptions(),true, VK_CULL_MODE_BACK_BIT, vulkan_state);

		return pipeline;
	}
	template<typename vertex_type>
	static pipeline create_shadow_pipeline(const std::string& vertex_filename,
		descriptor_set_layout descriptor_layout,
		VkRenderPass render_pass,
		const global_vulkan_instance_state& vulkan_state)
	{
		pipeline pipeline(vulkan_state, VK_POLYGON_MODE_FILL);

		pipeline.render_pass = render_pass;
		pipeline.shader_modules.push_back(shader_module::load_vertex_shader(vertex_filename, vulkan_state));

		pipeline.shader_descriptor_set_layout = descriptor_layout;
		pipeline.create_graphics_pipeline(vertex_type::get_binding_description(), vertex_type::get_attribute_descriptions(), false, VK_CULL_MODE_BACK_BIT, vulkan_state);

		return pipeline;
	}

};