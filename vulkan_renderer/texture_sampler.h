#pragma once
#include <vulkan\vulkan.h>
#include "vulkan_deleter.h"
#include "vulkan_common.h"

#include "global_vulkan_instance_state.h"
struct texture_sampler
{
	enum filter
	{
		linear,
		nearest
	};
	vulkan_deleter<VkSampler> sampler;
	filter filter_type;
	texture_sampler(){}
	texture_sampler(const global_vulkan_instance_state& vulkan_state)
	{
		sampler = { vulkan_state.logical_device, vkDestroySampler };
	}
	static texture_sampler create_clamped(filter sampler_filter, global_vulkan_instance_state& vulkan_state)
	{
		texture_sampler sampler{ vulkan_state };
		sampler.filter_type = sampler_filter;
		sampler.create_impl(sampler_filter, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, vulkan_state);
		return sampler;
	}
	static texture_sampler create(filter sampler_filter, global_vulkan_instance_state& vulkan_state)
	{
		texture_sampler sampler{ vulkan_state };
		sampler.filter_type = sampler_filter;
		sampler.create_impl(sampler_filter, VK_SAMPLER_ADDRESS_MODE_REPEAT, vulkan_state);
		return sampler;
	}
private:
	void create_impl(filter sampler_filter, VkSamplerAddressMode sampler_mode,  global_vulkan_instance_state& vulkan_state)
	{
		VkSamplerCreateInfo sampler_info{};
		sampler_info.sType		= VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		sampler_info.magFilter	= filter_as_vkfilter(sampler_filter);
		sampler_info.minFilter	= filter_as_vkfilter(sampler_filter);

		sampler_info.addressModeU = sampler_mode;
		sampler_info.addressModeV = sampler_mode;
		sampler_info.addressModeW = sampler_mode;

		sampler_info.anisotropyEnable			= vulkan_state.shader_details.enable_anisotropy;
		sampler_info.maxAnisotropy				= 1.0f;
		sampler_info.borderColor				= VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		sampler_info.unnormalizedCoordinates	= VK_FALSE;

		sampler_info.compareEnable	= VK_FALSE;
		sampler_info.compareOp		= VK_COMPARE_OP_ALWAYS;

		sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		sampler_info.mipLodBias = 0.f;
		sampler_info.minLod		= 0.f;
		sampler_info.maxLod		= 1.f;

		CHECK_VK_RESULT(vkCreateSampler(vulkan_state.logical_device, &sampler_info, nullptr, sampler.replace()));
	}
	VkFilter filter_as_vkfilter(filter sampler_filter)
	{
		switch (sampler_filter)
		{
		case linear:
			return VK_FILTER_LINEAR;
			break;
		case nearest:
			return VK_FILTER_NEAREST;
			break;
		}
		return VkFilter{};
	}
};