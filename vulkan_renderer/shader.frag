#version 450

layout(location = 0) in vec3 frag_normal;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec4 light_space_pos;
layout(location = 3) in vec4 light_direction;

layout(binding = 3) uniform sampler2D texSampler;
layout(binding = 4) uniform sampler2D shadow_sampler;

layout(location = 0) out vec4 outColor;

float compute_shadow_factor(vec4 light_space_pos, sampler2D shadow_map, uint shadow_map_size, uint pcf_size)
{
	const float max_shadow_darkness = 0.4f;
	const float max_brightness = 1.0f;
	vec3 light_space_ndc = light_space_pos.xyz/ light_space_pos.w;
	//if the light is entirely outside the projection this light can't affect it meaning its shadowed.
	if(	abs(light_space_ndc.x) >= 1.0f ||
		abs(light_space_ndc.y) >= 1.0f ||
		abs(light_space_ndc.z) >= 1.0f)
		return max_shadow_darkness;
	
	vec2 shadow_map_coord = light_space_ndc.xy*.5f + .5f;
	float bias = 0.005f*tan(acos(clamp(dot(frag_normal, light_direction.xyz),0,1)));
	bias = clamp(bias,0,0.02)/light_space_pos.w;
	// Check if the sample is in the light or in the shadow
   if ((light_space_ndc.z - bias) > texture(shadow_map, shadow_map_coord.xy).x)
      return max_shadow_darkness; // In the shadow

   // In the light
   return max_brightness;
	
	//float bias = 0.001f*tan(acos(clamp(dot(frag_normal, light_direction.xyz),0,1)));
	bias = clamp(bias, 0,0.01);
	float visibility = max_brightness;
	for(int i = 0; i < 4; ++i)
	{
		//if(texture(shadow_sampler, shadow_map_coord + poissonDisk[i]/8000.0f).z < (light_space_ndc.z-bias))
			visibility-= 0.2;
	}
	return visibility;
}
void main()
{
	vec4 color = texture(texSampler, vec2(frag_normal.x+1.f,frag_normal.y+1.f)/2);
	outColor = color*vec4(compute_shadow_factor(light_space_pos,shadow_sampler,1024,3));
	//outColor = texture(texSampler, vec2(frag_normal.x+1.f,frag_normal.y+1.f)/2);
}