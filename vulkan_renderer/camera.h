#pragma once
#include "vulkan_renderer.h"
namespace spawnx_vulkan
{
	struct camera
	{
		glm::mat4 view;
		glm::mat4 perspective;

		gpu_data_buffer camera_buffer;


		constexpr static VkDeviceSize buffer_size = sizeof(glm::mat4)*2;
		void set_world_pos(const glm::mat4& mat)
		{
			view = glm::inverse(mat);
		}
		static camera create_camera(const glm::mat4& perspective, global_vulkan_instance_state& vulkan_state)
		{
			camera cam{};
			cam.view = {};
			cam.perspective = perspective;
			cam.perspective[1][1] *= -1;
			cam.camera_buffer = gpu_data_buffer::create_host_visible_buffer(buffer_size, vulkan_state);
			return cam;
		}
		void update_buffer(const global_vulkan_instance_state& vulkan_state)
		{
			glm::mat4 data_to_copy[]{ view, perspective };
			memcpy(camera_buffer.allocation.alloc.data, &data_to_copy, sizeof(glm::mat4)*2);
		}
	};
}